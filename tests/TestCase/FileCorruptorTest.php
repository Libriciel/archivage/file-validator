<?php

namespace FileValidator\Test\TestCase;

use FileValidator\Utility\FileCorruptor;
use PHPUnit\Framework\TestCase;

class FileCorruptorTest extends TestCase
{
    public function testlowLevelCorruption()
    {
        $filename = tempnam(sys_get_temp_dir(), 'test-corruption-');
        file_put_contents($filename, 'test');
        $prev_sha256 = hash_file('sha256', $filename);
        file_put_contents($filename, FileCorruptor::lowLevelCorruption($filename));
        $next_sha256 = hash_file('sha256', $filename);
        unlink($filename);
        $this->assertNotEquals($prev_sha256, $next_sha256);
    }

    public function testhighLevelCorruption()
    {
        $filename = tempnam(sys_get_temp_dir(), 'test-corruption-');
        file_put_contents($filename, 'test');
        $prev_sha256 = hash_file('sha256', $filename);
        file_put_contents($filename, FileCorruptor::highLevelCorruption($filename));
        $next_sha256 = hash_file('sha256', $filename);
        unlink($filename);
        $this->assertNotEquals($prev_sha256, $next_sha256);
    }
}
