<?php

namespace FileValidator\Test\TestCase;

use FileValidator\Utility\FileValidator;
use PHPUnit\Framework\TestCase;

class FileValidatorTest extends TestCase
{
    public function testValidationArchive()
    {
        $path = SAMPLES_VALIDATOR.DS.'samples'.DS.'archive';
        foreach (glob($path.DS.'*') as $filename) {
            $basename = basename($filename);
            if (preg_match('/^broken_/', $basename)) {
                $this->assertFalse(FileValidator::check($filename), $basename);
            } elseif (preg_match('/^bad_/', $basename) === 0) {
                $this->assertTrue(FileValidator::check($filename), $basename);
            }
        }
    }

    public function testValidationAudio()
    {
        $path = SAMPLES_VALIDATOR.DS.'samples'.DS.'audio';
        foreach (glob($path.DS.'*') as $filename) {
            $basename = basename($filename);
            if (preg_match('/^broken_/', $basename)) {
                $this->assertFalse(FileValidator::check($filename), $basename);
            } elseif (preg_match('/^bad_/', $basename) === 0) {
                $this->assertTrue(FileValidator::check($filename), $basename);
            }
        }
    }

    public function testValidationDivers()
    {
        $path = SAMPLES_VALIDATOR.DS.'samples'.DS.'divers';
        foreach (glob($path.DS.'*') as $filename) {
            $basename = basename($filename);
            if (preg_match('/^broken_/', $basename)) {
                $this->assertFalse(FileValidator::check($filename), $basename);
            } elseif (preg_match('/^bad_/', $basename) === 0) {
                $this->assertTrue(FileValidator::check($filename), $basename);
            }
        }
    }

    public function testValidationExcel()
    {
        $path = SAMPLES_VALIDATOR.DS.'samples'.DS.'excel';
        foreach (glob($path.DS.'*') as $filename) {
            $basename = basename($filename);
            if (preg_match('/^broken_/', $basename)) {
                $this->assertFalse(FileValidator::check($filename), $basename);
            } elseif (preg_match('/^bad_/', $basename) === 0) {
                $this->assertTrue(FileValidator::check($filename), $basename);
            }
        }
    }

    public function testValidationImage()
    {
        $path = SAMPLES_VALIDATOR.DS.'samples'.DS.'image';
        foreach (glob($path.DS.'*') as $filename) {
            $basename = basename($filename);
            if (preg_match('/^broken_/', $basename)) {
                $this->assertFalse(FileValidator::check($filename), $basename);
            } elseif (preg_match('/^bad_/', $basename) === 0) {
                $this->assertTrue(FileValidator::check($filename), $basename);
            }
        }
    }

    public function testValidationPdf()
    {
        $path = SAMPLES_VALIDATOR.DS.'samples'.DS.'pdf';
        foreach (glob($path.DS.'*') as $filename) {
            $basename = basename($filename);
            if (preg_match('/^broken_/', $basename)) {
                $this->assertFalse(FileValidator::check($filename), $basename);
            } elseif (preg_match('/^bad_/', $basename) === 0) {
                $this->assertTrue(FileValidator::check($filename), $basename);
            }
        }
    }

    public function testValidationPowerpoint()
    {
        $path = SAMPLES_VALIDATOR.DS.'samples'.DS.'powerpoint';
        foreach (glob($path.DS.'*') as $filename) {
            $basename = basename($filename);
            if (preg_match('/^broken_/', $basename)) {
                $this->assertFalse(FileValidator::check($filename), $basename);
            } elseif (preg_match('/^bad_/', $basename) === 0) {
                $this->assertTrue(FileValidator::check($filename), $basename);
            }
        }
    }

    public function testValidationSpreadsheet()
    {
        $path = SAMPLES_VALIDATOR.DS.'samples'.DS.'spreadsheet';
        foreach (glob($path.DS.'*') as $filename) {
            $basename = basename($filename);
            if (preg_match('/^broken_/', $basename)) {
                $this->assertFalse(FileValidator::check($filename), $basename);
            } elseif (preg_match('/^bad_/', $basename) === 0) {
                $this->assertTrue(FileValidator::check($filename), $basename);
            }
        }
    }

    public function testValidationVideo()
    {
        $path = SAMPLES_VALIDATOR.DS.'samples'.DS.'video';
        foreach (glob($path.DS.'*') as $filename) {
            $basename = basename($filename);
            if (preg_match('/^broken_/', $basename)) {
                $this->assertFalse(FileValidator::check($filename), $basename);
            } elseif (preg_match('/^bad_/', $basename) === 0) {
                $this->assertTrue(FileValidator::check($filename), $basename);
            }
        }
    }

    public function testValidationWord()
    {
        $path = SAMPLES_VALIDATOR.DS.'samples'.DS.'word';
        foreach (glob($path.DS.'*') as $filename) {
            $basename = basename($filename);
            if (preg_match('/^broken_/', $basename)) {
                $this->assertFalse(FileValidator::check($filename), $basename);
            } elseif (preg_match('/^bad_/', $basename) === 0) {
                $this->assertTrue(FileValidator::check($filename), $basename);
            }
        }
    }

    public function testValidationXml()
    {
        $path = SAMPLES_VALIDATOR.DS.'samples'.DS.'xml';
        foreach (glob($path.DS.'*') as $filename) {
            $basename = basename($filename);
            if (preg_match('/^broken_/', $basename)) {
                $this->assertFalse(FileValidator::check($filename), $basename);
            } elseif (preg_match('/^bad_/', $basename) === 0) {
                $this->assertTrue(FileValidator::check($filename), $basename);
            }
        }
    }

    public function testNotFound()
    {
        $this->assertFalse(FileValidator::check('/'.uniqid('test-file-not-exists')));
    }

    public function testLoadConfig()
    {
        $initialConfig = FileValidator::$config;
        FileValidator::loadConfig([]);
        $this->assertEmpty(FileValidator::$config);
        FileValidator::loadConfig($initialConfig);
        $this->assertNotEmpty(FileValidator::$config);
    }

    public function testStaticCallbacks()
    {
        $this->assertTrue(FileValidator::isValid());
        $this->assertFalse(FileValidator::isError());
    }

    public function testXlsx()
    {
        $this->assertTrue(FileValidator::checkXlsx(SAMPLES_VALIDATOR.DS.'samples'.DS.'excel'.DS.'sample.xlsx'));
        $this->assertFalse(FileValidator::checkXlsx(SAMPLES_VALIDATOR.DS.'samples'.DS.'excel'.DS.'broken_fichier Excel 97-2003.xls'));
    }
}
