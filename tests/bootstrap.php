<?php
/**
 * Test runner bootstrap.
 *
 * Add additional configuration/setup your application needs when running
 * unit tests in this file.
 */

if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}
if (!defined('SAMPLES_VALIDATOR')) {
    define(
        'SAMPLES_VALIDATOR',
        ROOT_FILEVALIDATOR . DS . 'vendor' . DS . 'libriciel' . DS . 'file-validator-tests' . DS . 'tests'
    );
}
