<?php
/**
 * Bootstrap du plugin
 *
 * Inclut l'autoload du vendor, la configuration par défaut ainsi que le
 * lancement automatique des workers
 *
 * @category    Beanstalk
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2017, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */

use FileValidator\Utility\FileValidator;

FileValidator::loadConfig(require __DIR__.'/validations.php');

$basedir = dirname(__DIR__);
if (!is_dir($basedir.DIRECTORY_SEPARATOR.'vendor')) {
    $basedir = dirname(dirname(dirname($basedir)));
}
require $basedir . '/vendor/autoload.php';

if (!defined('ROOT_FILEVALIDATOR')) {
    define('ROOT_FILEVALIDATOR', $basedir);
}
