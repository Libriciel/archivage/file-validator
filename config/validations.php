<?php
/**
 * Configuration de la Validation d'un fichier.
 *
 * Modifier ce fichier afin d'utiliser un callback particulier pour un pronom
 * donné.
 * L'attribut callbacks est important. Si un seul des callbacks renvoi vrai, la
 * validation entiere du pronom est faite.
 *
 * @category    File
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2016, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */

return [
    'fmt/1' => [
        'name' => 'Broadcast WAVE',
        'other_name' => 'BWAVE (0), BWF (0)',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/2' => [
        'name' => 'Broadcast WAVE',
        'other_name' => 'BWAVE (1), BWF (1)',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/3' => [
        'name' => 'Graphics Interchange Format',
        'other_name' => 'GIF (1987a)',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/4' => [
        'name' => 'Graphics Interchange Format',
        'other_name' => 'GIF (1989a)',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/5' => [
        'name' => 'Audio/Video Interleaved Format',
        'other_name' => 'AVI (Generic)',
        'category' => 'Audio, Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
            1 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'fmt/6' => [
        'name' => 'Waveform Audio',
        'other_name' => 'WAVE',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/7' => [
        'name' => 'Tagged Image File Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/8' => [
        'name' => 'Tagged Image File Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/9' => [
        'name' => 'Tagged Image File Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/10' => [
        'name' => 'Tagged Image File Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/11' => [
        'name' => 'Portable Network Graphics',
        'other_name' => 'PNG (1.0)',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/12' => [
        'name' => 'Portable Network Graphics',
        'other_name' => 'PNG (1.1)',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/13' => [
        'name' => 'Portable Network Graphics',
        'other_name' => 'PNG (1.2)',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/14' => [
        'name' => 'Acrobat PDF 1.0 - Portable Document Format',
        'other_name' => 'PDF (1.0)',
        'category' => 'Page Description',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
        ],
    ],
    'fmt/15' => [
        'name' => 'Acrobat PDF 1.1 - Portable Document Format',
        'other_name' => 'PDF (1.1)',
        'category' => 'Page Description',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
        ],
    ],
    'fmt/16' => [
        'name' => 'Acrobat PDF 1.2 - Portable Document Format',
        'other_name' => 'PDF (1.2)',
        'category' => 'Page Description',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
        ],
    ],
    'fmt/17' => [
        'name' => 'Acrobat PDF 1.3 - Portable Document Format',
        'other_name' => 'PDF (1.3)',
        'category' => 'Page Description',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
        ],
    ],
    'fmt/18' => [
        'name' => 'Acrobat PDF 1.4 - Portable Document Format',
        'other_name' => 'PDF (1.4)',
        'category' => 'Page Description',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
        ],
    ],
    'fmt/19' => [
        'name' => 'Acrobat PDF 1.5 - Portable Document Format',
        'other_name' => 'PDF (1.5)',
        'category' => 'Page Description',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
        ],
    ],
    'fmt/20' => [
        'name' => 'Acrobat PDF 1.6 - Portable Document Format',
        'other_name' => 'PDF (1.6)',
        'category' => 'Page Description',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
        ],
    ],
    'fmt/21' => [
        'name' => 'AutoCAD Drawing',
        'other_name' => 'DWG (1.0)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/22' => [
        'name' => 'AutoCAD Drawing',
        'other_name' => 'DWG (1.2)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/23' => [
        'name' => 'AutoCAD Drawing',
        'other_name' => 'DWG (1.3)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/24' => [
        'name' => 'AutoCAD Drawing',
        'other_name' => 'DWG (1.4)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/25' => [
        'name' => 'AutoCAD Drawing',
        'other_name' => 'DWG (2.0)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/26' => [
        'name' => 'AutoCAD Drawing',
        'other_name' => 'DWG (2.1)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/27' => [
        'name' => 'AutoCAD Drawing',
        'other_name' => 'DWG (2.2)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/28' => [
        'name' => 'AutoCAD Drawing',
        'other_name' => 'DWG (2.5)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/29' => [
        'name' => 'AutoCAD Drawing',
        'other_name' => 'DWG (2.6)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/30' => [
        'name' => 'AutoCAD Drawing',
        'other_name' => 'DWG (R9)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/31' => [
        'name' => 'AutoCAD Drawing',
        'other_name' => 'DWG (R10)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/32' => [
        'name' => 'AutoCAD Drawing',
        'other_name' => 'DWG (R11/12)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/33' => [
        'name' => 'AutoCAD Drawing',
        'other_name' => 'DWG (R13)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/34' => [
        'name' => 'AutoCAD Drawing',
        'other_name' => 'DWG (R14)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/35' => [
        'name' => 'AutoCAD Drawing',
        'other_name' => 'DWG (2000-2002)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/36' => [
        'name' => 'AutoCAD Drawing',
        'other_name' => 'DWG (2004/2005/2006)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/37' => [
        'name' => 'Microsoft Word for Windows Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/38' => [
        'name' => 'Microsoft Word for Windows Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/39' => [
        'name' => 'Microsoft Word Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/40' => [
        'name' => 'Microsoft Word Document',
        'other_name' => 'Microsoft Word for Windows Document (97-XP)',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/41' => [
        'name' => 'Raw JPEG Stream',
        'other_name' => 'JPEG',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/42' => [
        'name' => 'JPEG File Interchange Format',
        'other_name' => 'JFIF (1.00)',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/43' => [
        'name' => 'JPEG File Interchange Format',
        'other_name' => 'JFIF (1.01)',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/44' => [
        'name' => 'JPEG File Interchange Format',
        'other_name' => 'JFIF (1.02)',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/45' => [
        'name' => 'Rich Text Format',
        'other_name' => 'RTF (1.0)',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/46' => [
        'name' => 'Rich Text Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/47' => [
        'name' => 'Rich Text Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/48' => [
        'name' => 'Rich Text Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/49' => [
        'name' => 'Rich Text Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/50' => [
        'name' => 'Rich Text Format',
        'other_name' => 'RTF (1.5), RTF (97)',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/51' => [
        'name' => 'Rich Text Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/52' => [
        'name' => 'Rich Text Format',
        'other_name' => 'RTF (1.7), RTF (XP), RTF (2002)',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/53' => [
        'name' => 'Rich Text Format',
        'other_name' => 'RTF (1.8), RTF (2003)',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/54' => [
        'name' => 'Drawing Interchange Binary Format',
        'other_name' => 'DXB (1.0)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/55' => [
        'name' => 'Microsoft Excel 2.x Worksheet (xls)',
        'other_name' => 'Microsoft Excel Worksheet (2.1)',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'fmt/56' => [
        'name' => 'Microsoft Excel 3.0 Worksheet (xls)',
        'other_name' => 'Microsoft Excel Worksheet (3.0)',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'fmt/57' => [
        'name' => 'Microsoft Excel 4.0 Worksheet (xls)',
        'other_name' => 'Microsoft Excel Worksheet (4.0)',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'fmt/58' => [
        'name' => 'Microsoft Excel 4.0 Workbook (xls)',
        'other_name' => 'Microsoft Excel Workbook (4.0)',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'fmt/59' => [
        'name' => 'Microsoft Excel 5.0/95 Workbook (xls)',
        'other_name' => 'Microsoft Excel Workbook (5.0)',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'fmt/60' => [
        'name' => 'Excel 95 Workbook (xls)',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'fmt/61' => [
        'name' => 'Microsoft Excel 97 Workbook (xls)',
        'other_name' => 'Microsoft Excel Workbook (97-2000)',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'fmt/62' => [
        'name' => 'Microsoft Excel 2000-2003 Workbook (xls)',
        'other_name' => 'Microsoft Excel Workbook (XP-2003), Microsoft Excel Workbook (2002-2003)',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'fmt/63' => [
        'name' => 'Drawing Interchange File Format (ASCII)',
        'other_name' => 'ASCII DXF (Generic)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/64' => [
        'name' => 'Drawing Interchange File Format (ASCII)',
        'other_name' => 'ASCII DXF (1.0)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/65' => [
        'name' => 'Drawing Interchange File Format (ASCII)',
        'other_name' => 'ASCII DXF (1.2)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/66' => [
        'name' => 'Drawing Interchange File Format (ASCII)',
        'other_name' => 'ASCII DXF (1.3)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/67' => [
        'name' => 'Drawing Interchange File Format (ASCII)',
        'other_name' => 'ASCII DXF (1.4)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/68' => [
        'name' => 'Drawing Interchange File Format (ASCII)',
        'other_name' => 'ASCII DXF (2.0)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/69' => [
        'name' => 'Drawing Interchange File Format (ASCII)',
        'other_name' => 'ASCII DXF (2.1)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/70' => [
        'name' => 'Drawing Interchange File Format (ASCII)',
        'other_name' => 'ASCII DXF (2.2)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/71' => [
        'name' => 'Drawing Interchange File Format (ASCII)',
        'other_name' => 'ASCII DXF (2.5)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/72' => [
        'name' => 'Drawing Interchange File Format (ASCII)',
        'other_name' => 'ASCII DXF (2.6)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/73' => [
        'name' => 'Drawing Interchange File Format (ASCII)',
        'other_name' => 'ASCII DXF (R9)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/74' => [
        'name' => 'Drawing Interchange File Format (ASCII)',
        'other_name' => 'ASCII DXF (R10)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/75' => [
        'name' => 'Drawing Interchange File Format (ASCII)',
        'other_name' => 'ASCII DXF (R11/12)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/76' => [
        'name' => 'Drawing Interchange File Format (ASCII)',
        'other_name' => 'ASCII DXF (R13)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/77' => [
        'name' => 'Drawing Interchange File Format (ASCII)',
        'other_name' => 'ASCII DXF (R14)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/78' => [
        'name' => 'Drawing Interchange File Format (ASCII)',
        'other_name' => 'ASCII DXF (2000-2002)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/79' => [
        'name' => 'Drawing Interchange File Format (ASCII)',
        'other_name' => 'ASCII DXF (2004-2005)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/80' => [
        'name' => 'Drawing Interchange File Format (Binary)',
        'other_name' => 'Binary DXF (R10)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/81' => [
        'name' => 'Drawing Interchange File Format (Binary)',
        'other_name' => 'Binary DXF (R11/12)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/82' => [
        'name' => 'Drawing Interchange File Format (Binary)',
        'other_name' => 'Binary DXF (R13)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/83' => [
        'name' => 'Drawing Interchange File Format (Binary)',
        'other_name' => 'Binary DXF (R14)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/84' => [
        'name' => 'Drawing Interchange File Format (Binary)',
        'other_name' => 'Binary DXF (2000-2002)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/85' => [
        'name' => 'Drawing Interchange File Format (Binary)',
        'other_name' => 'Binary DXF (2004-2005)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/86' => [
        'name' => 'PCX',
        'other_name' => 'ZSoft PC Paintbrush Bitmap (0)',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/87' => [
        'name' => 'PCX',
        'other_name' => 'ZSoft PC Paintbrush Bitmap (2)',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/88' => [
        'name' => 'PCX',
        'other_name' => 'ZSoft PC Paintbrush Bitmap (3)',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/89' => [
        'name' => 'PCX',
        'other_name' => 'ZSoft PC Paintbrush Bitmap (4)',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/90' => [
        'name' => 'PCX',
        'other_name' => 'ZSoft PC Paintbrush Bitmap (5)',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/91' => [
        'name' => 'Scalable Vector Graphics',
        'other_name' => 'SVG (1.0)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/92' => [
        'name' => 'Scalable Vector Graphics',
        'other_name' => 'SVG (1.1)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/93' => [
        'name' => 'Virtual Reality Modeling Language',
        'other_name' => 'VRML (1.0)',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/94' => [
        'name' => 'Virtual Reality Modeling Language',
        'other_name' => 'VRML (97), Virtual Reality Modeling Language (2.0), VRML (2.0)',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/95' => [
        'name' => 'Acrobat PDF/A - Portable Document Format',
        'other_name' => 'PDF/A (1)',
        'category' => 'Page Description',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
        ],
    ],
    'fmt/96' => [
        'name' => 'Hypertext Markup Language',
        'other_name' => 'HTML',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/97' => [
        'name' => 'Hypertext Markup Language',
        'other_name' => 'HTML (2.0)',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/98' => [
        'name' => 'Hypertext Markup Language',
        'other_name' => 'HTML (3.2)',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/99' => [
        'name' => 'Hypertext Markup Language',
        'other_name' => 'HTML (4.0)',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/100' => [
        'name' => 'Hypertext Markup Language',
        'other_name' => 'HTML (4.01)',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/101' => [
        'name' => 'Extensible Markup Language',
        'other_name' => 'XML (1.0)',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkXml',
        ],
    ],
    'fmt/102' => [
        'name' => 'Extensible Hypertext Markup Language',
        'other_name' => 'XHTML (1.0)',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/103' => [
        'name' => 'Extensible Hypertext Markup Language',
        'other_name' => 'XHTML (1.1)',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/104' => [
        'name' => 'Macromedia Flash',
        'other_name' => 'SWF (1)',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/105' => [
        'name' => 'Macromedia Flash',
        'other_name' => 'SWF (2)',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/106' => [
        'name' => 'Macromedia Flash',
        'other_name' => 'SWF (3)',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/107' => [
        'name' => 'Macromedia Flash',
        'other_name' => 'SWF (4)',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/108' => [
        'name' => 'Macromedia Flash',
        'other_name' => 'SWF (5)',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/109' => [
        'name' => 'Macromedia Flash',
        'other_name' => 'SWF (6)',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/110' => [
        'name' => 'Macromedia Flash',
        'other_name' => 'SWF (7)',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/111' => [
        'name' => 'OLE2 Compound Document Format',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/112' => [
        'name' => 'Still Picture Interchange File Format',
        'other_name' => 'SPIFF (1.0)',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/113' => [
        'name' => 'Still Picture Interchange File Format',
        'other_name' => 'SPIFF (2.0)',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/114' => [
        'name' => 'Windows Bitmap',
        'other_name' => 'BMP (1.0)',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/115' => [
        'name' => 'Windows Bitmap',
        'other_name' => 'BMP (2.0)',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/116' => [
        'name' => 'Windows Bitmap',
        'other_name' => 'BMP (3.0)',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/117' => [
        'name' => 'Windows Bitmap',
        'other_name' => 'BMP (3.0 NT)',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/118' => [
        'name' => 'Windows Bitmap',
        'other_name' => 'BMP (4.0)',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/119' => [
        'name' => 'Windows Bitmap',
        'other_name' => 'BMP (5.0)',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/120' => [
        'name' => 'DROID File Collection File Format',
        'other_name' => ' ',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/121' => [
        'name' => 'DROID Signature File Format',
        'other_name' => ' ',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/122' => [
        'name' => 'Encapsulated PostScript File Format',
        'other_name' => 'EPS (1.2)',
        'category' => 'Page Description',
        'callbacks' => [
        ],
    ],
    'fmt/123' => [
        'name' => 'Encapsulated PostScript File Format',
        'other_name' => 'EPS (2.0)',
        'category' => 'Page Description',
        'callbacks' => [
        ],
    ],
    'fmt/124' => [
        'name' => 'Encapsulated PostScript File Format',
        'other_name' => 'EPS (3.0)',
        'category' => 'Page Description',
        'callbacks' => [
        ],
    ],
    'fmt/125' => [
        'name' => 'Microsoft Powerpoint Presentation',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPowerpoint',
        ],
    ],
    'fmt/126' => [
        'name' => 'Microsoft Powerpoint Presentation',
        'other_name' => 'Microsoft Powerpoint Presentation (97-XP)',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPowerpoint',
        ],
    ],
    'fmt/127' => [
        'name' => 'OpenOffice Draw',
        'other_name' => 'StarOffice Draw (6.0)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/128' => [
        'name' => 'OpenOffice Writer',
        'other_name' => 'StarOffice Writer (1.0)',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/129' => [
        'name' => 'OpenOffice Calc',
        'other_name' => 'StarOffice Calc (6.0)',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/130' => [
        'name' => 'OpenOffice Impress',
        'other_name' => 'StarOffice Impress (6.0)',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/131' => [
        'name' => 'Advanced Systems Format',
        'other_name' => 'ASF',
        'category' => 'Audio, Image (Raster), Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
            1 => 'FileValidator\Utility\FileValidator::checkVideo',
            2 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/132' => [
        'name' => 'Windows Media Audio',
        'other_name' => 'WMA',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/133' => [
        'name' => 'Windows Media Video',
        'other_name' => 'WMV',
        'category' => 'Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'fmt/134' => [
        'name' => 'MPEG 1/2 Audio Layer 3',
        'other_name' => 'MP3',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/135' => [
        'name' => 'OpenDocument Format',
        'other_name' => 'ODF (1.0)',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/136' => [
        'name' => 'OpenDocument Text',
        'other_name' => 'ODF Text (1.0)',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/137' => [
        'name' => 'OpenDocument Spreadsheet',
        'other_name' => 'ODF Spreadsheet (1.0)',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/138' => [
        'name' => 'OpenDocument Presentation',
        'other_name' => 'ODF presentation (1.0)',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/139' => [
        'name' => 'OpenDocument Graphics',
        'other_name' => 'ODF Drawing (1.0)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/140' => [
        'name' => 'OpenDocument Database Format',
        'other_name' => 'ODF Database (1.0)',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/141' => [
        'name' => 'Waveform Audio (PCMWAVEFORMAT)',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/142' => [
        'name' => 'Waveform Audio (WAVEFORMATEX)',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/143' => [
        'name' => 'Waveform Audio (WAVEFORMATEXTENSIBLE)',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/144' => [
        'name' => 'Acrobat PDF/X - Portable Document Format - Exchange 1:1999',
        'other_name' => 'PDF/X-1:1999',
        'category' => 'Page Description',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
        ],
    ],
    'fmt/145' => [
        'name' => 'Acrobat PDF/X - Portable Document Format - Exchange 1:2001',
        'other_name' => 'PDF/X-1:2001',
        'category' => 'Page Description',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
        ],
    ],
    'fmt/146' => [
        'name' => 'Acrobat PDF/X - Portable Document Format - Exchange 1a:2003',
        'other_name' => 'PDF/X-1a:2003',
        'category' => 'Page Description',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
        ],
    ],
    'fmt/147' => [
        'name' => 'Acrobat PDF/X - Portable Document Format - Exchange 2:2003',
        'other_name' => 'PDF/X-2:2003',
        'category' => 'Page Description',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
        ],
    ],
    'fmt/148' => [
        'name' => 'Acrobat PDF/X - Portable Document Format - Exchange 3:2003',
        'other_name' => 'PDF/X-3:2003',
        'category' => 'Page Description',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
        ],
    ],
    'fmt/149' => [
        'name' => 'JTIP (JPEG Tiled Image Pyramid)',
        'other_name' => 'JTIP',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/150' => [
        'name' => 'JPEG-LS',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/151' => [
        'name' => 'JPX (JPEG 2000 part 2)',
        'other_name' => 'JPF',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/152' => [
        'name' => 'Digital Negative Format (DNG)',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/153' => [
        'name' => 'Tagged Image File Format for Image Technology (TIFF/IT)',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/154' => [
        'name' => 'Tagged Image File Format for Electronic Photography (TIFF/EP)',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/155' => [
        'name' => 'Geographic Tagged Image File Format (GeoTIFF)',
        'other_name' => ' ',
        'category' => 'GIS, Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/156' => [
        'name' => 'Tagged Image File Format for Internet Fax (TIFF-FX)',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/157' => [
        'name' => 'Acrobat PDF/X - Portable Document Format - Exchange 1a:2001',
        'other_name' => 'PDF/X-1a:2001',
        'category' => 'Page Description',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
        ],
    ],
    'fmt/158' => [
        'name' => 'Acrobat PDF/X - Portable Document Format - Exchange 3:2002',
        'other_name' => 'PDF/X-3:2002',
        'category' => 'Page Description',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
        ],
    ],
    'fmt/159' => [
        'name' => 'EBCDIC-US',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/160' => [
        'name' => 'TeX/LaTeX Device Independent Document',
        'other_name' => 'DVI',
        'category' => 'Page Description',
        'callbacks' => [
        ],
    ],
    'fmt/161' => [
        'name' => 'SIARD (Software-Independent Archiving of Relational Databases)',
        'other_name' => 'SIARD (2.0)',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/162' => [
        'name' => 'Microsoft Multiplan',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/163' => [
        'name' => 'Microsoft Works Word Processor 1-3 for DOS and 2 for Windows',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/164' => [
        'name' => 'Microsoft Works Word Processor for DOS',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/165' => [
        'name' => 'Microsoft Works Word Processor for DOS',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/166' => [
        'name' => 'Microsoft Works Spreadsheet',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/167' => [
        'name' => 'Microsoft Works Spreadsheet for DOS',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/168' => [
        'name' => 'Microsoft Works Spreadsheet for DOS',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/169' => [
        'name' => 'Microsoft Works Database for DOS',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/170' => [
        'name' => 'Microsoft Works Database for DOS',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/171' => [
        'name' => 'Microsoft Works Database for DOS',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/172' => [
        'name' => 'Microsoft Excel for Macintosh',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'fmt/173' => [
        'name' => 'Microsoft Excel for Macintosh',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'fmt/174' => [
        'name' => 'Microsoft Excel for Macintosh',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'fmt/175' => [
        'name' => 'Microsoft Excel for Macintosh',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'fmt/176' => [
        'name' => 'Microsoft Excel for Macintosh',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'fmt/177' => [
        'name' => 'Microsoft Excel for Macintosh',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'fmt/178' => [
        'name' => 'Microsoft Excel for Macintosh',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'fmt/179' => [
        'name' => 'Microsoft PowerPoint for Macintosh',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/180' => [
        'name' => 'Microsoft PowerPoint for Macintosh',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/181' => [
        'name' => 'Microsoft PowerPoint for Macintosh',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/182' => [
        'name' => 'Microsoft PowerPoint for Macintosh',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/183' => [
        'name' => 'PrimeOCR',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/184' => [
        'name' => 'PrimeOCR',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/185' => [
        'name' => 'Prime OCR',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/186' => [
        'name' => 'PrimeOCR',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/187' => [
        'name' => 'PrimeOCR',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/188' => [
        'name' => 'PrimeOCR',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/189' => [
        'name' => 'Microsoft Office Open XML',
        'other_name' => ' ',
        'category' => 'Aggregate',
        'callbacks' => [
        ],
    ],
    'fmt/190' => [
        'name' => 'Adobe FrameMaker Document',
        'other_name' => ' ',
        'category' => 'Presentation, Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
            1 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/191' => [
        'name' => 'Sony RAW Image File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/192' => [
        'name' => 'Kodak Digital Camera Raw Image File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/193' => [
        'name' => 'Digital Moving Picture Exchange Bitmap',
        'other_name' => ' ',
        'category' => 'Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'fmt/194' => [
        'name' => 'FileMaker Pro Ver. 7+ Database Document',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/195' => [
        'name' => 'ERDAS IMAGINE Gray-scale Bitmap Image',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/196' => [
        'name' => 'Adobe InDesign Document',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/197' => [
        'name' => 'InstallShield Compiled Rules File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/198' => [
        'name' => 'MPEG Audio Stream, Layer II',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/199' => [
        'name' => 'MPEG-4 Media File',
        'other_name' => ' ',
        'category' => 'Audio, Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
            1 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'fmt/200' => [
        'name' => 'Material Exchange Format',
        'other_name' => ' ',
        'category' => 'Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'fmt/201' => [
        'name' => 'Mathematica Notebook',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/202' => [
        'name' => 'Nikon Digital SLR Camera Raw Image File',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/203' => [
        'name' => 'Ogg Vorbis Codec Compressed Multimedia File',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/204' => [
        'name' => 'RealVideo Clip',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'fmt/205' => [
        'name' => 'Synchronized Multimedia Integration Language',
        'other_name' => ' ',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/206' => [
        'name' => 'Structured Query Language Data',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/207' => [
        'name' => 'Obsidium Project File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/208' => [
        'name' => 'Binary File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/209' => [
        'name' => 'Sound Designer II Audio File',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/210' => [
        'name' => 'Statistica Report File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/211' => [
        'name' => 'Kodak Photo CD Image',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/212' => [
        'name' => 'Information or Setup File',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/213' => [
        'name' => 'ScanIt Document',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/214' => [
        'name' => 'Microsoft Excel for Windows',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'fmt/215' => [
        'name' => 'Microsoft Powerpoint for Windows',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/216' => [
        'name' => 'Microsoft Visio XML Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/217' => [
        'name' => 'PaintShop Pro Browser Cache File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/218' => [
        'name' => 'Microsoft FrontPage',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/219' => [
        'name' => 'Microsoft Works Database for Windows',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/220' => [
        'name' => 'Microsoft Works Spreadsheet for Windows',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/221' => [
        'name' => 'Microsoft Works Word Processor for Windows',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/222' => [
        'name' => 'Microsoft Works Database for Windows',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/223' => [
        'name' => 'Microsoft Works Database for Windows',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/224' => [
        'name' => 'Microsoft Works Database for Windows',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/225' => [
        'name' => 'Microsoft Works Database for Windows',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/226' => [
        'name' => 'Microsoft Works Database for Windows',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/227' => [
        'name' => 'Microsoft Works Spreadsheet for Windows',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/228' => [
        'name' => 'Microsoft Works Spreadsheet for Windows',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/229' => [
        'name' => 'Microsoft Works Spreadsheet for Windows',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/230' => [
        'name' => 'Microsoft Works Spreadsheet for Windows',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/231' => [
        'name' => 'Microsoft Works Spreadsheet for Windows',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/232' => [
        'name' => 'Microsoft Works Word Processor for Windows',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/233' => [
        'name' => 'Microsoft Works Word Processor 3-4 for Windows',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/234' => [
        'name' => 'Microsoft Works Word Processor for Windows',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/235' => [
        'name' => 'Microsoft Works Word Processor for Windows',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/236' => [
        'name' => 'Microsoft Works Word Processor for Windows',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/237' => [
        'name' => 'Microsoft Office Binder File for Windows',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/238' => [
        'name' => 'Microsoft Office Binder Template for Windows',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/239' => [
        'name' => 'Microsoft Office Binder Wizard for Windows',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/240' => [
        'name' => 'Microsoft Office Binder File for Windows',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/241' => [
        'name' => 'Microsoft Office Binder Template for Windows',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/242' => [
        'name' => 'Microsoft Office Binder Wizard for Windows',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/243' => [
        'name' => 'GPS eXchange Format',
        'other_name' => ' ',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/244' => [
        'name' => 'Keyhole Markup Language (XML)',
        'other_name' => ' ',
        'category' => 'GIS, Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/245' => [
        'name' => 'Structured Data Exchange Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/246' => [
        'name' => 'Microsoft Works Database for Windows',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/247' => [
        'name' => 'Microsoft Works Spreadsheet for Windows',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/248' => [
        'name' => 'Microsoft Works Word Processor Windows',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/249' => [
        'name' => 'Microsoft Works Database for Windows',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/250' => [
        'name' => 'Microsoft Works Spreadsheet for Windows',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/251' => [
        'name' => 'Microsoft Works Word Processor Windows',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/252' => [
        'name' => 'Microsoft Works Database for Windows',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/253' => [
        'name' => 'Microsoft Works Spreadsheet for Windows',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/254' => [
        'name' => 'Microsoft Works Word Processor Windows',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/255' => [
        'name' => 'DjVu File Format',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/256' => [
        'name' => 'Microsoft Works Database for Windows',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/257' => [
        'name' => 'Microsoft Works Spreadsheet for Windows',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/258' => [
        'name' => 'Microsoft Works Word Processor 5-6',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/259' => [
        'name' => 'Microsoft Works Database for DOS',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/260' => [
        'name' => 'Microsoft Works Database for DOS',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/261' => [
        'name' => 'Microsoft Works Database for DOS',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/262' => [
        'name' => 'Microsoft Works Spreadsheet for DOS',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/263' => [
        'name' => 'Microsoft Works Spreadsheet for DOS',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/264' => [
        'name' => 'Microsoft Works Spreadsheet for DOS',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/265' => [
        'name' => 'Microsoft Works Word Processor DOS',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/266' => [
        'name' => 'Microsoft Works Word Processor DOS',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/267' => [
        'name' => 'Microsoft Works Word Processor DOS',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/268' => [
        'name' => 'Microsoft Works Database for Macintosh',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/269' => [
        'name' => 'Microsoft Works Database for Macintosh',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/270' => [
        'name' => 'Microsoft Works Spreadsheet for Macintosh',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/271' => [
        'name' => 'Microsoft Works Spreadsheet for Macintosh',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/272' => [
        'name' => 'Microsoft Works Word Processor Macintosh',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/273' => [
        'name' => 'Microsoft Works Word Processor Macintosh',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/274' => [
        'name' => 'SPSS Output File (spv)',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/275' => [
        'name' => 'Microsoft Access Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/276' => [
        'name' => 'Acrobat PDF 1.7 - Portable Document Format',
        'other_name' => ' ',
        'category' => 'Page Description',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
        ],
    ],
    'fmt/277' => [
        'name' => 'ESRI Arc/View Shapefile Index',
        'other_name' => ' ',
        'category' => 'GIS',
        'callbacks' => [
        ],
    ],
    'fmt/278' => [
        'name' => 'Internet Message Format',
        'other_name' => ' ',
        'category' => 'Email',
        'callbacks' => [
        ],
    ],
    'fmt/279' => [
        'name' => 'FLAC (Free Lossless Audio Codec)',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/280' => [
        'name' => 'LaTeX (Master document)',
        'other_name' => ' ',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/281' => [
        'name' => 'LaTeX (Subdocument)',
        'other_name' => ' ',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/282' => [
        'name' => 'netCDF-3 Classic',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/283' => [
        'name' => 'netCDF-3 64-bit',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/284' => [
        'name' => 'Gridded Binary',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/285' => [
        'name' => 'Gridded Binary',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/286' => [
        'name' => 'HDF5',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/287' => [
        'name' => 'HDF5',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/288' => [
        'name' => 'Microsoft Front Page Server Extension Configuration',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/289' => [
        'name' => 'WARC',
        'other_name' => 'ISO 28500-2009, Web ARChive file format',
        'category' => 'Aggregate',
        'callbacks' => [
        ],
    ],
    'fmt/290' => [
        'name' => 'OpenDocument Text',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/291' => [
        'name' => 'OpenDocument Text',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/292' => [
        'name' => 'OpenDocument Presentation',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/293' => [
        'name' => 'OpenDocument Presentation',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/294' => [
        'name' => 'OpenDocument Spreadsheet',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/295' => [
        'name' => 'OpenDocument Spreadsheet',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/296' => [
        'name' => 'OpenDocument Graphics',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/297' => [
        'name' => 'OpenDocument Graphics',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/298' => [
        'name' => 'Autodesk Animator Pro FLIC',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/299' => [
        'name' => 'Autodesk Animator (FlicLib)',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/300' => [
        'name' => 'ChiWriter Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/301' => [
        'name' => 'Computer Graphics Metafile ASCII',
        'other_name' => ' ',
        'category' => 'Image (Raster), Image (Vector), Text (Structured)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/302' => [
        'name' => 'Computer Graphics Metafile ASCII',
        'other_name' => ' ',
        'category' => 'Image (Raster), Image (Vector), Text (Structured)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/303' => [
        'name' => 'Computer Graphics Metafile (Binary)',
        'other_name' => ' ',
        'category' => 'Image (Raster), Image (Vector), Text (Structured)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/304' => [
        'name' => 'Computer Graphics Metafile (Binary)',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/305' => [
        'name' => 'Computer Graphics Metafile (Binary)',
        'other_name' => ' ',
        'category' => 'Image (Raster), Image (Vector), Text (Structured)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/306' => [
        'name' => 'Computer Graphics Metafile (Binary)',
        'other_name' => ' ',
        'category' => 'Image (Raster), Image (Vector), Text (Structured)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/307' => [
        'name' => 'Quicken Interchange Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/308' => [
        'name' => 'Quicken Data Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/309' => [
        'name' => 'Open Financial Exchange',
        'other_name' => ' ',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/310' => [
        'name' => 'Open Financial Exchange',
        'other_name' => ' ',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/311' => [
        'name' => 'Open Financial Exchange',
        'other_name' => ' ',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/312' => [
        'name' => 'Open Financial Exchange',
        'other_name' => ' ',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/313' => [
        'name' => 'Open Financial Exchange',
        'other_name' => ' ',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/314' => [
        'name' => 'Play SID Audio',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/315' => [
        'name' => 'Play SID Audio',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/316' => [
        'name' => 'Real SID Audio',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/317' => [
        'name' => 'Macromedia Director',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/318' => [
        'name' => 'Secure DjVU',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/319' => [
        'name' => 'ESRI Spatial Index File',
        'other_name' => ' ',
        'category' => 'GIS',
        'callbacks' => [
        ],
    ],
    'fmt/320' => [
        'name' => 'ESRI Shapefile Projection (Well-Known Text) Format',
        'other_name' => ' ',
        'category' => 'GIS',
        'callbacks' => [
        ],
    ],
    'fmt/321' => [
        'name' => 'ESRI Shapefile Header Index',
        'other_name' => ' ',
        'category' => 'GIS',
        'callbacks' => [
        ],
    ],
    'fmt/322' => [
        'name' => 'Portable Form File',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/323' => [
        'name' => 'Extended Module Audio File',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/324' => [
        'name' => 'EndNote Style File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/325' => [
        'name' => 'EndNote Library',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/326' => [
        'name' => 'EndNote Connection File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/327' => [
        'name' => 'EndNote Filter File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/328' => [
        'name' => 'EndNote Import File',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/329' => [
        'name' => 'Shell Archive Format',
        'other_name' => ' ',
        'category' => 'Aggregate',
        'callbacks' => [
        ],
    ],
    'fmt/330' => [
        'name' => 'Peak Graphical Waveform File',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/331' => [
        'name' => 'Autorun Configuration File',
        'other_name' => ' ',
        'category' => 'Text (Unstructured)',
        'callbacks' => [
        ],
    ],
    'fmt/332' => [
        'name' => 'ESRI Arc/View Project',
        'other_name' => ' ',
        'category' => 'GIS',
        'callbacks' => [
        ],
    ],
    'fmt/333' => [
        'name' => 'Chemical Markup Language',
        'other_name' => ' ',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/334' => [
        'name' => 'Crystallographic Information Framework',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/335' => [
        'name' => 'Dreamweaver Lock File',
        'other_name' => ' ',
        'category' => 'Text (Unstructured)',
        'callbacks' => [
        ],
    ],
    'fmt/336' => [
        'name' => 'Graphic Workshop for Windows Thumbnail File',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/337' => [
        'name' => 'MJ2 (Motion JPEG 2000)',
        'other_name' => ' ',
        'category' => 'Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'fmt/338' => [
        'name' => 'Interchange File Format Interleaved Bitmap',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/339' => [
        'name' => 'Interchange File Format 8-bit Sampled Voice',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/340' => [
        'name' => 'Lotus WordPro Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/341' => [
        'name' => 'Macintosh PICT Image',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/342' => [
        'name' => 'Microsoft Project Export File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/343' => [
        'name' => 'Microsoft Project Export File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/344' => [
        'name' => 'Microsoft Windows Enhanced Metafile',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/345' => [
        'name' => 'Microsoft Windows Enhanced Metafile',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/346' => [
        'name' => 'Microsoft Word for Macintosh Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/347' => [
        'name' => 'MPEG 1/2 Audio Layer I',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/348' => [
        'name' => 'Paint Shop Pro Image',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/349' => [
        'name' => 'Paint Shop Pro Image',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/350' => [
        'name' => 'Paradox Database Table',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/351' => [
        'name' => 'Paradox Database Table',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/352' => [
        'name' => 'Paradox Database Table',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/353' => [
        'name' => 'Tagged Image File Format',
        'other_name' => 'TIFF',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/354' => [
        'name' => 'Acrobat PDF/A - Portable Document Format',
        'other_name' => ' ',
        'category' => 'Page Description',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
        ],
    ],
    'fmt/355' => [
        'name' => 'Rich Text Format',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/356' => [
        'name' => 'Adaptive Multi-Rate Audio',
        'other_name' => 'AMR-NB, GSM-AMR',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/357' => [
        'name' => '3GPP Audio/Video File',
        'other_name' => ' ',
        'category' => 'Audio, Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
            1 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'fmt/358' => [
        'name' => 'Internet Data Query File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/359' => [
        'name' => 'Microsoft Front Page Binary Tree Index',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/360' => [
        'name' => 'pulse EKKO data file',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/361' => [
        'name' => 'pulse EKKO header file',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/362' => [
        'name' => 'GSSI SIR-10 RADAN data file',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/363' => [
        'name' => 'SEG Y Data Exchange Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/364' => [
        'name' => 'National Imagery Transmission Format',
        'other_name' => ' ',
        'category' => 'Image (Raster), Aggregate',
        'callbacks' => [
        ],
    ],
    'fmt/365' => [
        'name' => 'National Imagery Transmission Format',
        'other_name' => ' ',
        'category' => 'Image (Raster), Aggregate',
        'callbacks' => [
        ],
    ],
    'fmt/366' => [
        'name' => 'National Imagery Transmission Format',
        'other_name' => ' ',
        'category' => 'Image (Raster), Aggregate',
        'callbacks' => [
        ],
    ],
    'fmt/367' => [
        'name' => 'ESRI World File Format',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/368' => [
        'name' => 'ASPRS Lidar Data Exchange Format',
        'other_name' => ' ',
        'category' => 'GIS, Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/369' => [
        'name' => 'ASPRS Lidar Data Exchange Format',
        'other_name' => ' ',
        'category' => 'GIS, Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/370' => [
        'name' => 'ASPRS Lidar Data Exchange Format',
        'other_name' => ' ',
        'category' => 'GIS, Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/371' => [
        'name' => 'Enhanced Compression Wavelet',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/372' => [
        'name' => 'Earth Resource Satellite image header format',
        'other_name' => ' ',
        'category' => 'GIS, Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/373' => [
        'name' => 'FoxPro Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/374' => [
        'name' => 'Microsoft Visual FoxPro Database Table File',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/375' => [
        'name' => 'FoxPro Compound Index File',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/376' => [
        'name' => 'FoxPro Report',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/377' => [
        'name' => 'Microsoft Visual FoxPro Report',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/378' => [
        'name' => 'Chemical Draw Exchange Format',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/379' => [
        'name' => 'Microsoft Visual FoxPro Class Library',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/380' => [
        'name' => 'Microsoft Visual FoxPro Project',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/381' => [
        'name' => 'FoxPro Project',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/382' => [
        'name' => 'Microsoft Visual FoxPro database container (table files)',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/383' => [
        'name' => 'VICAR (Video Image Communication and Retrieval) Planetary File Format',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/384' => [
        'name' => 'Microsoft Visual FoxPro database container (memo files)',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/385' => [
        'name' => 'Microsoft Windows Cursor',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/386' => [
        'name' => 'Microsoft Animated Cursor Format',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/387' => [
        'name' => 'VCalendar format',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/388' => [
        'name' => 'Internet Calendar and Scheduling format',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/389' => [
        'name' => 'Log ASCII Standard Format',
        'other_name' => ' ',
        'category' => 'Text (Structured), Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/390' => [
        'name' => 'Log ASCII Standard Format',
        'other_name' => ' ',
        'category' => 'Text (Structured), Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/391' => [
        'name' => 'Log ASCII Standard Format',
        'other_name' => ' ',
        'category' => 'Text (Structured), Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/392' => [
        'name' => 'MrSID Image Format (Multi-resolution Seamless Image Database)',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/393' => [
        'name' => 'Borland Reflex flat datafile',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/394' => [
        'name' => 'DS_store file (MAC)',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/395' => [
        'name' => 'vCard',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/396' => [
        'name' => 'PocketMobi (Palm Resource) File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/397' => [
        'name' => 'Enigma Binary File (Finale)',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/398' => [
        'name' => 'Enigma Transportable File (Finale)',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/399' => [
        'name' => 'Stuffit X Archive File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/400' => [
        'name' => 'Macromedia FreeHand MX',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/401' => [
        'name' => 'X-Windows Screen Dump',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/402' => [
        'name' => 'Truevision TGA Bitmap',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/403' => [
        'name' => 'SuperCalc Spreadsheet',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/404' => [
        'name' => 'RealAudio',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/405' => [
        'name' => 'Portable Any Map',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/406' => [
        'name' => 'Portable Grey Map - Binary',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/407' => [
        'name' => 'Portable Grey Map - ASCII',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/408' => [
        'name' => 'Portable Pixel Map - Binary',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/409' => [
        'name' => 'Portable Bitmap Image - Binary',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/410' => [
        'name' => 'Internet Archive',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/411' => [
        'name' => 'RAR Archive',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkRar',
        ],
    ],
    'fmt/412' => [
        'name' => 'Microsoft Word for Windows',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/413' => [
        'name' => 'Scalable Vector Graphics Tiny',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/414' => [
        'name' => 'Audio Interchange File Format',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/415' => [
        'name' => 'Cinema 4D',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/416' => [
        'name' => 'Apple Core Audio Format',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/417' => [
        'name' => 'Adobe Illustrator',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/418' => [
        'name' => 'Adobe Illustrator',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/419' => [
        'name' => 'Adobe Illustrator',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/420' => [
        'name' => 'Adobe Illustrator',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/421' => [
        'name' => 'Adobe Illustrator',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/422' => [
        'name' => 'Adobe Illustrator',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/423' => [
        'name' => 'Adobe Illustrator',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/424' => [
        'name' => 'OpenDocument Database Format',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/425' => [
        'name' => 'Video Object File (MPEG-2 subset)',
        'other_name' => ' ',
        'category' => 'Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'fmt/426' => [
        'name' => 'Harris Matrix',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/427' => [
        'name' => 'CorelDraw Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/428' => [
        'name' => 'CorelDraw Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/429' => [
        'name' => 'CorelDraw Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/430' => [
        'name' => 'CorelDraw Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/431' => [
        'name' => 'Corel R.A.V.E.',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/432' => [
        'name' => 'Corel R.A.V.E.',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/433' => [
        'name' => 'Drawing Interchange File Format (ASCII)',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/434' => [
        'name' => 'AutoCAD Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/435' => [
        'name' => 'Drawing Interchange File Format (ASCII)',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/436' => [
        'name' => 'Digital Negative Format (DNG)',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/437' => [
        'name' => 'Digital Negative Format (DNG)',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/438' => [
        'name' => 'Digital Negative Format (DNG)',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/439' => [
        'name' => 'BSDIFF',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/440' => [
        'name' => 'Microsoft Project',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/441' => [
        'name' => 'Windows Media Video 9 Advanced Profile (WVC1)',
        'other_name' => ' ',
        'category' => 'Audio, Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
            1 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'fmt/442' => [
        'name' => 'Microsoft Visio (generic)',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/443' => [
        'name' => 'Microsoft Visio Drawing',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/444' => [
        'name' => 'OpenDocument Database Format',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/445' => [
        'name' => 'Microsoft Excel Macro-Enabled',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'fmt/446' => [
        'name' => 'Adobe Portable Document Catalog Index File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/447' => [
        'name' => 'Adobe Portable Document Catalog Index File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/448' => [
        'name' => 'Adobe Portable Document Catalog Index File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/449' => [
        'name' => 'Adobe Portable Document Catalog Index File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/450' => [
        'name' => 'VectorWorks',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/451' => [
        'name' => 'VectorWorks',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/452' => [
        'name' => 'Acrobat Catalog Cat File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/453' => [
        'name' => 'Verity Collection Stop List',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/454' => [
        'name' => 'Verity Collection Index About File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/455' => [
        'name' => 'Verity Collection Index Pending Transaction File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/456' => [
        'name' => 'Verity Collection Index Style Policy',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/457' => [
        'name' => 'Verity Collection Document Dataset Descriptor Style Set',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/458' => [
        'name' => 'Verity Collection Document Index Descriptor Style Set',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/459' => [
        'name' => 'Verity Collection Word List Descriptor Style Set',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/460' => [
        'name' => 'Verity Collection Partition Definition Descriptor Style Set',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/461' => [
        'name' => 'Verity Collection Index Descriptor File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/462' => [
        'name' => 'MS DOS Compression format',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/463' => [
        'name' => 'JPM (JPEG 2000 part 6)',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/464' => [
        'name' => 'CorelDraw Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/465' => [
        'name' => 'CorelDraw Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/466' => [
        'name' => 'CorelDraw Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/467' => [
        'name' => 'CorelDraw Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/468' => [
        'name' => 'ISO Disk Image File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/469' => [
        'name' => 'MS DOS Compression format (KWAJ variant)',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/470' => [
        'name' => 'Asymetrix Toolbook File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/471' => [
        'name' => 'Hypertext Markup Language',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/472' => [
        'name' => 'Sony Digital Voice File/Sony Memory Stick Voice File',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/473' => [
        'name' => 'Microsoft Office Owner File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/474' => [
        'name' => 'Windows Help File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/475' => [
        'name' => 'Microsoft Management Console Snap-in Control file',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/476' => [
        'name' => 'Acrobat PDF/A - Portable Document Format',
        'other_name' => ' ',
        'category' => 'Page Description',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
        ],
    ],
    'fmt/477' => [
        'name' => 'Acrobat PDF/A - Portable Document Format',
        'other_name' => ' ',
        'category' => 'Page Description',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
        ],
    ],
    'fmt/478' => [
        'name' => 'Acrobat PDF/A - Portable Document Format',
        'other_name' => ' ',
        'category' => 'Page Description',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
        ],
    ],
    'fmt/479' => [
        'name' => 'Acrobat PDF/A - Portable Document Format',
        'other_name' => ' ',
        'category' => 'Page Description',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
        ],
    ],
    'fmt/480' => [
        'name' => 'Acrobat PDF/A - Portable Document Format',
        'other_name' => ' ',
        'category' => 'Page Description',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
        ],
    ],
    'fmt/481' => [
        'name' => 'Acrobat PDF/A - Portable Document Format',
        'other_name' => ' ',
        'category' => 'Page Description',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
        ],
    ],
    'fmt/482' => [
        'name' => 'Apple iBook format',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/483' => [
        'name' => 'ePub format',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/484' => [
        'name' => '7Zip format',
        'other_name' => ' ',
        'category' => 'Aggregate',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkGzip',
        ],
    ],
    'fmt/485' => [
        'name' => 'Rocket Book eBook format',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/486' => [
        'name' => 'Macromedia (Adobe) Director Compressed Resource file',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/487' => [
        'name' => 'Macro Enabled Microsoft Powerpoint',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/488' => [
        'name' => 'Acrobat PDF/X - Portable Document Format - Exchange PDF/X-4',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
            1 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/489' => [
        'name' => 'Acrobat PDF/X - Portable Document Format - Exchange PDF/X-4p',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
            1 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/490' => [
        'name' => 'Acrobat PDF/X - Portable Document Format - Exchange PDF/X-5g',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
            1 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/491' => [
        'name' => 'Acrobat PDF/X - Portable Document Format - Exchange PDF/X-5pg',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
            1 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/492' => [
        'name' => 'Acrobat PDF/X - Portable Document Format - Exchange PDF/X-5n',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
            1 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/493' => [
        'name' => 'Acrobat PDF/E - Portable Document Format for Engineering PDF/E-1',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPdf',
            1 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/494' => [
        'name' => 'Microsoft Office Encrypted Document',
        'other_name' => ' ',
        'category' => 'Presentation, Spreadsheet, Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
            1 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
            2 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/495' => [
        'name' => 'ATCO-CIF',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/496' => [
        'name' => 'TransXchange File Format',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/497' => [
        'name' => 'Wireless Bitmap',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/498' => [
        'name' => 'ActiveX License Package file',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/499' => [
        'name' => 'VivoActive',
        'other_name' => ' ',
        'category' => 'Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'fmt/500' => [
        'name' => 'Internet Explorer for Mac cache file',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/501' => [
        'name' => 'PostScript',
        'other_name' => ' ',
        'category' => 'Page Description',
        'callbacks' => [
        ],
    ],
    'fmt/502' => [
        'name' => 'Bentley V8 DGN',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/503' => [
        'name' => 'AppleDouble Resource Fork',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/504' => [
        'name' => 'Standard Flowgram Format',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/505' => [
        'name' => 'Adobe Flash',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/506' => [
        'name' => 'Adobe Flash',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/507' => [
        'name' => 'Adobe Flash',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/508' => [
        'name' => 'Quarter Inch Cartridge Host Interchange Format',
        'other_name' => ' ',
        'category' => 'Aggregate',
        'callbacks' => [
        ],
    ],
    'fmt/509' => [
        'name' => 'Adobe PostScript Font Metrics file',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/510' => [
        'name' => 'PowerProject Teamplan',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/511' => [
        'name' => 'PowerProject',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/512' => [
        'name' => 'PowerProject',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/513' => [
        'name' => 'PowerProject',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/514' => [
        'name' => 'PowerProject',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/515' => [
        'name' => 'PowerProject',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/516' => [
        'name' => 'PowerProject',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/517' => [
        'name' => 'PowerProject',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/518' => [
        'name' => 'Broad Band eBook',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/519' => [
        'name' => 'Polynomial Texture Map',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/520' => [
        'name' => 'OpenType Font File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/521' => [
        'name' => 'Adobe Multiple Master Metrics font file',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/522' => [
        'name' => 'Open Project File',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/523' => [
        'name' => 'Macro enabled Microsoft Word Document OOXML',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/524' => [
        'name' => 'Microsoft Office Theme',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/525' => [
        'name' => 'Adobe Printer Font Binary',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/526' => [
        'name' => 'Adobe Font List',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/527' => [
        'name' => 'Broadcast WAVE',
        'other_name' => 'BWAVE (2), BWF (2)',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/528' => [
        'name' => 'Multiple-image Network Graphics',
        'other_name' => ' ',
        'category' => 'Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'fmt/529' => [
        'name' => 'JPEG Network Graphics',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/530' => [
        'name' => 'eRuby HTML document',
        'other_name' => ' ',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/531' => [
        'name' => 'AutoCAD Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/532' => [
        'name' => 'Drawing Interchange File Format (ASCII)',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/533' => [
        'name' => 'Adobe FrameMaker Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/534' => [
        'name' => 'Adobe FrameMaker Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/535' => [
        'name' => 'Adobe FrameMaker Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/536' => [
        'name' => 'Adobe FrameMaker Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/537' => [
        'name' => 'Adobe FrameMaker Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/538' => [
        'name' => 'Adobe FrameMaker Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/539' => [
        'name' => 'Adobe FrameMaker Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/540' => [
        'name' => 'Cinema 4D',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/541' => [
        'name' => 'Digital Moving Picture Exchange Bitmap',
        'other_name' => ' ',
        'category' => 'Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'fmt/542' => [
        'name' => 'GEM Metafile Format',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/543' => [
        'name' => 'GEM Metafile Format',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/544' => [
        'name' => 'Macromedia FreeHand',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/545' => [
        'name' => 'Macromedia FreeHand',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/546' => [
        'name' => 'Macromedia FreeHand',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/547' => [
        'name' => 'Macromedia FreeHand',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/548' => [
        'name' => 'Adobe InDesign Document',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/549' => [
        'name' => 'Adobe InDesign Document',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/550' => [
        'name' => 'Adobe InDesign Document',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/551' => [
        'name' => 'Adobe InDesign Document',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/552' => [
        'name' => 'Adobe InDesign Document',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/553' => [
        'name' => 'Microsoft Excel Chart',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'fmt/554' => [
        'name' => 'Microsoft Excel Chart',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'fmt/555' => [
        'name' => 'Microsoft Excel Macro',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'fmt/556' => [
        'name' => 'Microsoft Excel Macro',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'fmt/557' => [
        'name' => 'Adobe Illustrator',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/558' => [
        'name' => 'Adobe Illustrator',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/559' => [
        'name' => 'Adobe Illustrator',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/560' => [
        'name' => 'Adobe Illustrator',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/561' => [
        'name' => 'Adobe Illustrator',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/562' => [
        'name' => 'Adobe Illustrator',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/563' => [
        'name' => 'Adobe Illustrator',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/564' => [
        'name' => 'Adobe Illustrator',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/565' => [
        'name' => 'Adobe Illustrator',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/566' => [
        'name' => 'WebP',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/567' => [
        'name' => 'WebP',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/568' => [
        'name' => 'WebP',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/569' => [
        'name' => 'Matroska',
        'other_name' => ' ',
        'category' => 'Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
            1 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'fmt/570' => [
        'name' => 'Extensible Metadata Platform Packet',
        'other_name' => ' ',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/571' => [
        'name' => 'Domino XML Document Export',
        'other_name' => ' ',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/572' => [
        'name' => 'Domino XML Database Export',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/573' => [
        'name' => 'WebM',
        'other_name' => ' ',
        'category' => 'Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'fmt/574' => [
        'name' => 'Digital Imaging and Communications in Medicine File Format',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/575' => [
        'name' => 'GraphPad Prism',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/576' => [
        'name' => 'GraphPad Prism',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/577' => [
        'name' => 'Image Cytometry Standard',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/578' => [
        'name' => 'Image Cytometry Standard',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/579' => [
        'name' => 'X3D',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/580' => [
        'name' => 'X3D',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/581' => [
        'name' => 'X3D',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/582' => [
        'name' => 'X3D',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/583' => [
        'name' => 'Vector Markup Language',
        'other_name' => ' ',
        'category' => 'Image (Vector), Text (Mark-up)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/584' => [
        'name' => 'Windows Media Metafile',
        'other_name' => ' ',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/585' => [
        'name' => 'MPEG-2 Transport Stream',
        'other_name' => ' ',
        'category' => 'Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'fmt/586' => [
        'name' => 'LifeTechnologies SDS',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/587' => [
        'name' => 'LifeTechnologies ABIF',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/588' => [
        'name' => 'R3D',
        'other_name' => ' ',
        'category' => 'Audio, Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
            1 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'fmt/589' => [
        'name' => 'Windows Media Playlist',
        'other_name' => ' ',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/590' => [
        'name' => 'JPEG Extended Range',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/591' => [
        'name' => 'Radiance RGBE Image Format',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/592' => [
        'name' => 'Canon RAW',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/593' => [
        'name' => 'Canon RAW',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/594' => [
        'name' => 'Microsoft PhotoDraw',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/595' => [
        'name' => 'Microsoft Excel Non-XML Binary Workbook',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'fmt/596' => [
        'name' => 'Apple Lossless Audio Codec',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/597' => [
        'name' => 'Microsoft Word Template',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/598' => [
        'name' => 'Microsoft Excel Template',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'fmt/599' => [
        'name' => 'Microsoft Word Macro-Enabled Document Template',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/600' => [
        'name' => 'eXtensible ARchive format',
        'other_name' => ' ',
        'category' => 'Aggregate',
        'callbacks' => [
        ],
    ],
    'fmt/601' => [
        'name' => 'Statistical Analysis System Catalogue XPT (Windows)',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/602' => [
        'name' => 'Statistical Analysis System Catalogue XPT (Unix)',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/603' => [
        'name' => 'Statistical Analysis System Data XPT (Windows)',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/604' => [
        'name' => 'Statistical Analysis System Data XPT (Unix)',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/605' => [
        'name' => 'Statistical Analysis System Catalogue (Windows)',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/606' => [
        'name' => 'Statistical Analysis System Catalogue (Unix)',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/607' => [
        'name' => 'Statistical Analysis System Data (Windows)',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/608' => [
        'name' => 'Statistical Analysis System Data (Unix)',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/609' => [
        'name' => 'Microsoft Word (Generic)',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/610' => [
        'name' => 'ARJ File Format',
        'other_name' => ' ',
        'category' => 'Aggregate',
        'callbacks' => [
        ],
    ],
    'fmt/611' => [
        'name' => 'LDAP Data Interchange Format',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/612' => [
        'name' => 'Mork',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/613' => [
        'name' => 'RAR Archive',
        'other_name' => ' ',
        'category' => 'Aggregate',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkRar',
        ],
    ],
    'fmt/614' => [
        'name' => 'Windows Imaging Format',
        'other_name' => ' ',
        'category' => 'Aggregate',
        'callbacks' => [
        ],
    ],
    'fmt/615' => [
        'name' => 'Gimp Image File Format',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/616' => [
        'name' => 'Web Open Font Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/617' => [
        'name' => 'GeoGebra',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/618' => [
        'name' => 'GeoGebra',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/619' => [
        'name' => 'GeoGebra',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/620' => [
        'name' => 'GeoGebra',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/621' => [
        'name' => 'GeoGebra',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/622' => [
        'name' => 'GeoGebra',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/623' => [
        'name' => 'SmartDraw',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/624' => [
        'name' => 'RIFF Palette Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/625' => [
        'name' => 'Apple Disk Copy Image',
        'other_name' => ' ',
        'category' => 'Aggregate',
        'callbacks' => [
        ],
    ],
    'fmt/626' => [
        'name' => 'LHA File Format',
        'other_name' => ' ',
        'category' => 'Aggregate',
        'callbacks' => [
        ],
    ],
    'fmt/627' => [
        'name' => 'Microsoft Excel Macro-Enabled Template',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'fmt/628' => [
        'name' => 'Microsoft Excel Macro-Enabled Add-In',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'fmt/629' => [
        'name' => 'Microsoft PowerPoint Show',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/630' => [
        'name' => 'Microsoft PowerPoint Macro-Enabled Show',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/631' => [
        'name' => 'Microsoft PowerPoint Template',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/632' => [
        'name' => 'Microsoft PowerPoint Macro-Enabled Template',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/633' => [
        'name' => 'Microsoft PowerPoint Macro-Enabled Add-In',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/634' => [
        'name' => 'Microsoft Compiled HTML Help',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/635' => [
        'name' => 'CPIO',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/636' => [
        'name' => 'Microsoft PowerPoint Macro-Enabled Slide',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/637' => [
        'name' => 'Microsoft OneNote',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/638' => [
        'name' => 'SPSS Data File',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/639' => [
        'name' => 'Stuffit Archive File',
        'other_name' => ' ',
        'category' => 'Aggregate',
        'callbacks' => [
        ],
    ],
    'fmt/640' => [
        'name' => 'MPEG-2 Elementary Stream',
        'other_name' => ' ',
        'category' => 'Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'fmt/641' => [
        'name' => 'Epson Raw Image Format',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/642' => [
        'name' => 'Fujifilm RAW Image Format',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/643' => [
        'name' => 'ASTM E57 3D File Format',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/644' => [
        'name' => 'Nullsoft Scriptable Install System',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/645' => [
        'name' => 'Exchangeable Image File Format (Compressed)',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/646' => [
        'name' => 'Apple iWorks Keynote',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/647' => [
        'name' => 'Microsoft Expression Media',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/648' => [
        'name' => 'Media View Pro',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/649' => [
        'name' => 'MPEG-1 Elementary Stream',
        'other_name' => ' ',
        'category' => 'Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'fmt/650' => [
        'name' => 'Quark Xpress Report File',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/651' => [
        'name' => 'Quark Xpress Data File',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/652' => [
        'name' => 'Quark Xpress Data File',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/653' => [
        'name' => 'INTERLIS Transfer File',
        'other_name' => ' ',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/654' => [
        'name' => 'INTERLIS Model File',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/655' => [
        'name' => 'KryoFlux',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/656' => [
        'name' => 'KryoFlux',
        'other_name' => ' ',
        'category' => 'Aggregate',
        'callbacks' => [
        ],
    ],
    'fmt/657' => [
        'name' => 'Open XML Paper Specification',
        'other_name' => ' ',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/658' => [
        'name' => 'Cypher Query Language',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/659' => [
        'name' => 'Industry Foundation Classes',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/660' => [
        'name' => 'Adobe Type 1 Mac Font File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/661' => [
        'name' => 'Sigma RAW Image',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/662' => [
        'name' => 'Panasonic Raw',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/663' => [
        'name' => 'Industry Foundation Classes XML',
        'other_name' => ' ',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/664' => [
        'name' => 'Gerber Format',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/665' => [
        'name' => 'Chasys Draw image file',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/666' => [
        'name' => 'ART image format',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/667' => [
        'name' => 'Photoshop Curve File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/668' => [
        'name' => 'Olympus RAW',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/669' => [
        'name' => 'Minolta RAW',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/670' => [
        'name' => 'PKCS #7 Crytographic message file',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/671' => [
        'name' => 'Serif PagePlus Publication',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/672' => [
        'name' => 'Serif PagePlus Publication',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/673' => [
        'name' => 'Serif PagePlus Publication',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/674' => [
        'name' => 'Serif PagePlus Publication',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/675' => [
        'name' => 'Serif PagePlus Publication',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/676' => [
        'name' => 'Serif PagePlus Publication',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/677' => [
        'name' => 'Serif PagePlus Publication',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/678' => [
        'name' => 'Serif PagePlus Publication',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/679' => [
        'name' => 'Serif PagePlus Publication',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/680' => [
        'name' => 'Serif PagePlus Publication',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/681' => [
        'name' => 'Serif PagePlus Publication',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/682' => [
        'name' => 'Thumbs DB file',
        'other_name' => ' ',
        'category' => 'Image (Raster), Aggregate',
        'callbacks' => [
        ],
    ],
    'fmt/683' => [
        'name' => 'Advanced Function Presentation',
        'other_name' => ' ',
        'category' => 'Page Description',
        'callbacks' => [
        ],
    ],
    'fmt/684' => [
        'name' => 'Vectorworks',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/685' => [
        'name' => 'Quark Xpress Data File',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/686' => [
        'name' => 'Vectorworks',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/687' => [
        'name' => 'Better Portable Graphics',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/688' => [
        'name' => 'Executable and Linkable Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/689' => [
        'name' => 'Executable and Linkable Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/690' => [
        'name' => 'Executable and Linkable Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/691' => [
        'name' => 'Executable and Linkable Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/692' => [
        'name' => 'Mach-O',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/693' => [
        'name' => 'Mach-O',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/694' => [
        'name' => 'Dalvik Executable Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/695' => [
        'name' => 'Optimised Dalvik Executable Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/696' => [
        'name' => 'Sibelius',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/697' => [
        'name' => 'Additive Manufacturing File Format',
        'other_name' => ' ',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/698' => [
        'name' => 'Standard for the Exchange of Product model data',
        'other_name' => 'STEP',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/699' => [
        'name' => 'Industry Foundation Classes',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/700' => [
        'name' => 'Industry Foundation Classes',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/701' => [
        'name' => 'Processing Development Environment',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/702' => [
        'name' => 'Universal 3D File Format',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/703' => [
        'name' => 'Broadcast WAVE',
        'other_name' => 'BWAVE (0), BWF (0)',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/704' => [
        'name' => 'Broadcast WAVE',
        'other_name' => 'BWAVE (1), BWF (1)',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/705' => [
        'name' => 'Broadcast WAVE',
        'other_name' => 'BWAVE (2), BWF (2)',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/706' => [
        'name' => 'Broadcast WAVE',
        'other_name' => 'BWAVE (0), BWF (0)',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/707' => [
        'name' => 'Broadcast WAVE',
        'other_name' => 'BWAVE (1), BWF (1)',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/708' => [
        'name' => 'Broadcast WAVE',
        'other_name' => 'BWAVE (2), BWF (2)',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/709' => [
        'name' => 'Broadcast WAVE',
        'other_name' => 'BWAVE (0), BWF (0)',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/710' => [
        'name' => 'Broadcast WAVE',
        'other_name' => 'BWAVE (1), BWF (1)',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/711' => [
        'name' => 'Broadcast WAVE',
        'other_name' => 'BWAVE (2), BWF (2)',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/712' => [
        'name' => 'RF64',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/713' => [
        'name' => 'RF64 Multichannel Broadcast Wave format',
        'other_name' => 'MBWF',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/714' => [
        'name' => 'Extensible Music Format',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/715' => [
        'name' => 'Impulse Tracker Module',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/716' => [
        'name' => 'MOD Audio Module',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/717' => [
        'name' => 'Scream Tracker Module',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/718' => [
        'name' => 'Scream Tracker Module',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/719' => [
        'name' => 'MultiTracker Module',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/720' => [
        'name' => 'MBOX',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/721' => [
        'name' => 'VLW Font File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/722' => [
        'name' => 'Oktalyzer Audio file',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/723' => [
        'name' => 'Farandole Composer Module',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/724' => [
        'name' => 'Keyhole Markup Language (Container)',
        'other_name' => ' ',
        'category' => 'GIS',
        'callbacks' => [
        ],
    ],
    'fmt/725' => [
        'name' => 'Microsoft Project',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/726' => [
        'name' => 'Virtual Disk Image',
        'other_name' => ' ',
        'category' => 'Aggregate',
        'callbacks' => [
        ],
    ],
    'fmt/727' => [
        'name' => 'Cartesian Perceptual Compression image format',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/728' => [
        'name' => 'RealLegal E-Transcript',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/729' => [
        'name' => 'SQLite Database File Format',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/730' => [
        'name' => 'Digital Negative Format (DNG)',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/731' => [
        'name' => 'Bink Video Format',
        'other_name' => ' ',
        'category' => 'Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'fmt/732' => [
        'name' => 'Bink Video Format',
        'other_name' => ' ',
        'category' => 'Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'fmt/733' => [
        'name' => 'FL Studio project file (FLP)',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/734' => [
        'name' => 'SuperScape Virtual Reality Format',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/735' => [
        'name' => 'Dolby Digital AC-3',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/736' => [
        'name' => 'ClarisWorks',
        'other_name' => ' ',
        'category' => 'Database, Image (Raster), Image (Vector), Spreadsheet, Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
            1 => 'FileValidator\Utility\FileValidator::checkWord',
            2 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/737' => [
        'name' => 'ClarisWorks',
        'other_name' => ' ',
        'category' => 'Database, Image (Raster), Image (Vector), Spreadsheet, Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
            1 => 'FileValidator\Utility\FileValidator::checkWord',
            2 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/738' => [
        'name' => 'ClarisWorks Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/739' => [
        'name' => 'ClarisWorks Word Processor',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/740' => [
        'name' => 'ClarisWorks Spreadsheet',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/741' => [
        'name' => 'ClarisWorks Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/742' => [
        'name' => 'ClarisWorks Painting',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/743' => [
        'name' => 'ClarisWorks/AppleWorks Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/744' => [
        'name' => 'ClarisWorks/AppleWorks Word Processor',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/745' => [
        'name' => 'ClarisWorks/AppleWorks Spreadsheet',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/746' => [
        'name' => 'ClarisWorks/AppleWorks Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/747' => [
        'name' => 'ClarisWorks/AppleWorks Painting',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/748' => [
        'name' => 'AppleWorks Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/749' => [
        'name' => 'AppleWorks Word Processor',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/750' => [
        'name' => 'AppleWorks Spreadsheet',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/751' => [
        'name' => 'AppleWorks Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/752' => [
        'name' => 'AppleWorks Painting',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/753' => [
        'name' => 'AppleWorks Presentation',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/754' => [
        'name' => 'Microsoft Word Document (Password Protected)',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/755' => [
        'name' => 'Microsoft Word Document Template (Password Protected)',
        'other_name' => ' ',
        'category' => 'Text (Unstructured), Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/756' => [
        'name' => 'Zope export file',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/757' => [
        'name' => 'Adobe Flash',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/758' => [
        'name' => 'Adobe Flash',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/759' => [
        'name' => 'Adobe Flash',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/760' => [
        'name' => 'Adobe Flash',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/761' => [
        'name' => 'Adobe Flash',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/762' => [
        'name' => 'Adobe Flash',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/763' => [
        'name' => 'Adobe Flash',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/764' => [
        'name' => 'Adobe Flash',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/765' => [
        'name' => 'Adobe Flash',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/766' => [
        'name' => 'Adobe Flash',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/767' => [
        'name' => 'Adobe Flash',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/768' => [
        'name' => 'Adobe Flash',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/769' => [
        'name' => 'Adobe Flash',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/770' => [
        'name' => 'Adobe Flash',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/771' => [
        'name' => 'Adobe Flash',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/772' => [
        'name' => 'Adobe Flash',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/773' => [
        'name' => 'Adobe Flash',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/774' => [
        'name' => 'Adobe Flash',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/775' => [
        'name' => 'Adobe Flash',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/776' => [
        'name' => 'Adobe Flash',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/777' => [
        'name' => 'Microsoft Network Monitor Packet Capture',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/778' => [
        'name' => 'Microsoft Network Monitor Packet Capture',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/779' => [
        'name' => 'pcap Packet Capture',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/780' => [
        'name' => 'pcap Next Generation Packet Capture',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/781' => [
        'name' => 'Snoop Packet Capture',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/782' => [
        'name' => 'PowerVR Object Data',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/783' => [
        'name' => 'Material Exchange Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/784' => [
        'name' => 'Material Exchange Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/785' => [
        'name' => 'Material Exchange Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/786' => [
        'name' => 'Material Exchange Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/787' => [
        'name' => 'Material Exchange Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/788' => [
        'name' => 'Material Exchange Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/789' => [
        'name' => 'Material Exchange Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/790' => [
        'name' => 'Material Exchange Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/791' => [
        'name' => 'Material Exchange Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/792' => [
        'name' => 'Unified Emulator Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/793' => [
        'name' => 'RPM Package Manager file',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/794' => [
        'name' => 'RPM Package Manager file',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/795' => [
        'name' => 'RPM Package Manager file',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/796' => [
        'name' => 'Adobe After Effects',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/797' => [
        'name' => 'Apple ProRes',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/798' => [
        'name' => 'The Neuroimaging Informatics Technology Initiative File format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/799' => [
        'name' => 'WriteNow',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/800' => [
        'name' => 'CSV Schema',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/801' => [
        'name' => 'TAP (ZX Spectrum)',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/802' => [
        'name' => 'TAP (Commodore 64)',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/803' => [
        'name' => 'Encase Image File Format/Expert Witness Compression Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/804' => [
        'name' => 'Logical File Evidence Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/805' => [
        'name' => 'XAML Binary Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/806' => [
        'name' => 'Level 5 MAT-file format (MATLAB)',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/807' => [
        'name' => 'HDF5',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/808' => [
        'name' => 'StarOffice Calc',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/809' => [
        'name' => 'StarOffice Calc',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/810' => [
        'name' => 'StarOffice Draw',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/811' => [
        'name' => 'StarOffice Draw',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/812' => [
        'name' => 'StarOffice Writer',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/813' => [
        'name' => 'StarOffice Writer',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/814' => [
        'name' => 'StarOffice Impress',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/815' => [
        'name' => 'StarOffice Impress',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/816' => [
        'name' => 'NUT Open Container Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/817' => [
        'name' => 'JSON Data Interchange Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/818' => [
        'name' => 'YAML',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/819' => [
        'name' => 'CD-ROM/XA (eXtended Architecture)',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/820' => [
        'name' => 'T64 Tape Image Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/821' => [
        'name' => 'G64 GCR-encoded Disk Image Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/822' => [
        'name' => 'CRT C64 Cartridge Image Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/823' => [
        'name' => 'P00 C64 Image Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/824' => [
        'name' => 'Apple iWork Pages',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/825' => [
        'name' => 'Apple iWork Numbers',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/826' => [
        'name' => 'Scriptware Script Format',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/827' => [
        'name' => 'Serif DrawPlus Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/828' => [
        'name' => 'MAT-file format',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/829' => [
        'name' => '3MF 3D Manufacturing Format',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/830' => [
        'name' => 'Qsplat Model',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/831' => [
        'name' => 'Polygon File Format',
        'other_name' => 'Stanford Triangle Format',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/832' => [
        'name' => 'Open Inventor File Format',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/833' => [
        'name' => 'Open Inventor File Format',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/834' => [
        'name' => 'Quattro Pro Spreadsheet for Windows',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/835' => [
        'name' => 'Quattro Pro Spreadsheet for Windows',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/836' => [
        'name' => 'Quattro Pro Spreadsheet',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/837' => [
        'name' => 'Quattro Pro Spreadsheet',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/838' => [
        'name' => 'Outlook Express Message Database',
        'other_name' => ' ',
        'category' => 'Email',
        'callbacks' => [
        ],
    ],
    'fmt/839' => [
        'name' => 'Outlook Express Folder Database',
        'other_name' => ' ',
        'category' => 'Email',
        'callbacks' => [
        ],
    ],
    'fmt/840' => [
        'name' => 'ADX Audio Format',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/841' => [
        'name' => 'Interleaved ADX Audio Format (AIX)',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/842' => [
        'name' => 'AccessData Custom Content Image',
        'other_name' => ' ',
        'category' => 'Aggregate',
        'callbacks' => [
        ],
    ],
    'fmt/843' => [
        'name' => 'AccessData Custom Content Image (Encrypted)',
        'other_name' => ' ',
        'category' => 'Aggregate',
        'callbacks' => [
        ],
    ],
    'fmt/844' => [
        'name' => 'Advanced Forensic Format',
        'other_name' => ' ',
        'category' => 'Aggregate',
        'callbacks' => [
        ],
    ],
    'fmt/845' => [
        'name' => 'ClarisWorks Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/846' => [
        'name' => 'ClarisWorks Word Processor',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/847' => [
        'name' => 'ClarisWorks Spreadsheet',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/848' => [
        'name' => 'ClarisWorks Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/849' => [
        'name' => 'ClarisWorks Painting',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/850' => [
        'name' => 'NuFile Exchange Archival Library',
        'other_name' => 'ShrinkIt (NuFX) document',
        'category' => 'Aggregate',
        'callbacks' => [
        ],
    ],
    'fmt/851' => [
        'name' => 'Genealogical Data Communication (GEDCOM) Format',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/852' => [
        'name' => 'Serif DrawPlus Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/853' => [
        'name' => 'Serif DrawPlus Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/854' => [
        'name' => 'Personal Ancestral File (PAF)',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/855' => [
        'name' => 'Personal Ancestral File (PAF)',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/856' => [
        'name' => 'Personal Ancestral File (PAF)',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/857' => [
        'name' => 'Navisworks Document',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/858' => [
        'name' => 'Navisworks Document',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/859' => [
        'name' => 'Navisworks Document',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/860' => [
        'name' => 'Navisworks Document',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/861' => [
        'name' => 'Maya Binary File Format',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/862' => [
        'name' => 'Maya Binary File Format',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/863' => [
        'name' => 'Maya ASCII File Format',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/864' => [
        'name' => '3DM',
        'other_name' => 'openNURBS (5), Rhino (5)',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/865' => [
        'name' => 'STL (Standard Tessellation Language) Binary',
        'other_name' => 'Stereolithography file (Binary)',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/866' => [
        'name' => 'Apple Safari Webarchive',
        'other_name' => ' ',
        'category' => 'Aggregate',
        'callbacks' => [
        ],
    ],
    'fmt/867' => [
        'name' => 'Microsoft Reader eBook',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/868' => [
        'name' => 'MySQL Table Definition Format',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'fmt/869' => [
        'name' => 'CDX Internet Archive Index',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/870' => [
        'name' => 'Perl Script',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/871' => [
        'name' => 'Adobe Content Server Message File',
        'other_name' => ' ',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/872' => [
        'name' => 'Free Lossless Image Format (FLIF)',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/873' => [
        'name' => 'Notation3',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/874' => [
        'name' => 'Turtle',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/875' => [
        'name' => 'RDF/XML',
        'other_name' => ' ',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/876' => [
        'name' => 'Pagemaker Document (Generic)',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/877' => [
        'name' => 'Corel Presentation',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/878' => [
        'name' => 'Corel Presentation',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/879' => [
        'name' => 'Fortran',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/880' => [
        'name' => 'JSON-LD',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/881' => [
        'name' => 'Microsoft Document Imaging File Format',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/882' => [
        'name' => 'Wordstar 2000',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/883' => [
        'name' => 'Siegfried Signature File',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/884' => [
        'name' => 'AXD HTTP Handler File',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/885' => [
        'name' => 'BASIC File',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/886' => [
        'name' => 'HTML Components',
        'other_name' => ' ',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'fmt/887' => [
        'name' => 'SafeGuard Encrypted Virtual Disk',
        'other_name' => ' ',
        'category' => 'Aggregate',
        'callbacks' => [
        ],
    ],
    'fmt/888' => [
        'name' => 'QuadriSpace Format',
        'other_name' => ' ',
        'category' => 'Page Description, Model',
        'callbacks' => [
        ],
    ],
    'fmt/889' => [
        'name' => 'Feather',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/890' => [
        'name' => 'AbiWord Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/891' => [
        'name' => 'AbiWord Document Template',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/892' => [
        'name' => 'Compound WordPerfect for Windows Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/893' => [
        'name' => 'i2 Analysts Notebook',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/894' => [
        'name' => 'Gaussian Input Data File',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/895' => [
        'name' => 'JEOL NMR Spectroscopy',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/896' => [
        'name' => 'Music XML',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/897' => [
        'name' => 'Compressed Music XML',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/898' => [
        'name' => 'Zoomify Image Format',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/899' => [
        'name' => 'Windows Portable Executable',
        'other_name' => 'Common Object File Format, COFF, EXE',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/900' => [
        'name' => 'Windows Portable Executable',
        'other_name' => 'Common Object File Format, COFF, EXE',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/901' => [
        'name' => 'Microsoft Works Spreadsheet',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'fmt/902' => [
        'name' => 'Blender 3D',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/903' => [
        'name' => 'Blender 3D',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/904' => [
        'name' => 'Bluetooth Snoop Packet Capture',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/905' => [
        'name' => 'Variant Call Format',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/906' => [
        'name' => 'Variant Call Format',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/907' => [
        'name' => 'Variant Call Format',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/908' => [
        'name' => 'Variant Call Format',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/909' => [
        'name' => 'CRAM File Format',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/910' => [
        'name' => 'CRAM File Format',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/911' => [
        'name' => 'CRAM File Format',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/912' => [
        'name' => 'Microsoft Paint',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/913' => [
        'name' => 'Caligari trueSpace File Format',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/914' => [
        'name' => 'Caligari trueSpace File Format',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'fmt/915' => [
        'name' => 'Mapsforge Binary Map File Format',
        'other_name' => ' ',
        'category' => 'GIS',
        'callbacks' => [
        ],
    ],
    'fmt/916' => [
        'name' => 'ESRI ArcMap Document',
        'other_name' => ' ',
        'category' => 'GIS',
        'callbacks' => [
        ],
    ],
    'fmt/917' => [
        'name' => 'AmiraMesh',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/918' => [
        'name' => 'AmiraMesh',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/919' => [
        'name' => 'AmiraMesh',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/920' => [
        'name' => 'AmiraMesh',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/921' => [
        'name' => 'AmiraMesh',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/922' => [
        'name' => 'Xar Image Format',
        'other_name' => 'Flare Image Format',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/923' => [
        'name' => 'Microsoft xWMA',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/924' => [
        'name' => 'Microsoft Visio Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector), Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
            1 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/925' => [
        'name' => 'Microsoft Visio Stencil',
        'other_name' => ' ',
        'category' => 'Image (Vector), Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
            1 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/926' => [
        'name' => 'Microsoft Visio Template',
        'other_name' => ' ',
        'category' => 'Image (Vector), Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
            1 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/927' => [
        'name' => 'Microsoft Visio Macro-Enabled Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector), Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
            1 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/928' => [
        'name' => 'Microsoft Visio Macro-Enabled Stencil',
        'other_name' => ' ',
        'category' => 'Image (Vector), Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
            1 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/929' => [
        'name' => 'Microsoft Visio Macro-Enabled Template',
        'other_name' => ' ',
        'category' => 'Image (Vector), Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
            1 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'fmt/930' => [
        'name' => 'Magick Image File Format',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/931' => [
        'name' => 'Mathcad Document',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/932' => [
        'name' => 'Mathcad Document',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/933' => [
        'name' => 'Simple Vector Format',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/934' => [
        'name' => 'Simple Vector Format',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/935' => [
        'name' => 'Animated Portable Network Graphics',
        'other_name' => 'APNG',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/936' => [
        'name' => 'Microsoft Picture It! Image File',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/937' => [
        'name' => 'Adobe Air',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/938' => [
        'name' => 'Python Script File',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/939' => [
        'name' => 'Python Compiled File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/940' => [
        'name' => 'Python Compiled File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/941' => [
        'name' => 'Back Up File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/942' => [
        'name' => 'Adobe Air',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/943' => [
        'name' => 'Adobe Air',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/944' => [
        'name' => 'Ogg Multimedia Container',
        'other_name' => ' ',
        'category' => 'Audio, Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
            1 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'fmt/945' => [
        'name' => 'Ogg Theora Video',
        'other_name' => ' ',
        'category' => 'Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'fmt/946' => [
        'name' => 'Ogg Opus Codec Compressed Multimedia File',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/947' => [
        'name' => 'Ogg FLAC Compressed Multimedia File',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/948' => [
        'name' => 'Ogg Speex Codec Multimedia File',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/949' => [
        'name' => 'WordPerfect',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/950' => [
        'name' => 'MIME Email',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'fmt/951' => [
        'name' => 'Sonic Foundry WAVE 64',
        'other_name' => 'Sony Media WAVE 64',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/952' => [
        'name' => 'True Audio',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/953' => [
        'name' => 'True Audio',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/954' => [
        'name' => 'Adaptive Multi-Rate Wideband Audio',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/955' => [
        'name' => 'Downloadable Sounds Audio',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/956' => [
        'name' => 'RIFF-based MIDI',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/957' => [
        'name' => 'DirectMusic Segment File Format',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/958' => [
        'name' => 'DirectMusic Style File Format',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/959' => [
        'name' => 'Portable Sound Format',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/960' => [
        'name' => 'DOS Sound and Music Interface Advanced Module Format',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/961' => [
        'name' => 'Mobile eXtensible Music Format',
        'other_name' => 'Mobile XMF',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/962' => [
        'name' => 'QCP Audio File Format',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/963' => [
        'name' => 'OMNIC Spectral Data File',
        'other_name' => 'Thermo Scientific OMNIC Data File',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'fmt/964' => [
        'name' => 'Final Draft Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/965' => [
        'name' => 'Music Encoding Initiative',
        'other_name' => ' ',
        'category' => 'Audio, Text (Mark-up)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/966' => [
        'name' => 'AppleDouble Resource Fork',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/967' => [
        'name' => 'AppleSingle',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/968' => [
        'name' => 'AppleSingle',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'fmt/969' => [
        'name' => '	Rich Text Format',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'fmt/970' => [
        'name' => 'Khronos Texture File',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'fmt/971' => [
        'name' => 'Microsoft Windows Movie Maker File',
        'other_name' => ' ',
        'category' => 'Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'fmt/972' => [
        'name' => 'Dolby MLP Lossless Audio',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/973' => [
        'name' => 'DTS Coherent Acoustics (DCA) Audio',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/974' => [
        'name' => 'Notation Interchange File Format',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'fmt/975' => [
        'name' => '',
        'other_name' => '',
        'category' => '',
        'callbacks' => [
        ],
    ],
    'x-fmt/1' => [
        'name' => 'Microsoft Word for Macintosh Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/2' => [
        'name' => 'Microsoft Word for Macintosh Document',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/3' => [
        'name' => 'Online Description Tool Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/4' => [
        'name' => 'Write for Windows Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/5' => [
        'name' => 'Works for Macintosh Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/6' => [
        'name' => 'FoxPro Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/7' => [
        'name' => 'FoxPro Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/8' => [
        'name' => 'dBASE Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/9' => [
        'name' => 'dBASE Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/10' => [
        'name' => 'dBASE Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/11' => [
        'name' => 'Revisable-Form-Text Document Content Architecture',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/12' => [
        'name' => 'Write for Windows Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/13' => [
        'name' => 'Tab-separated values',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'x-fmt/14' => [
        'name' => 'Macintosh Text File',
        'other_name' => ' ',
        'category' => 'Text (Unstructured)',
        'callbacks' => [
        ],
    ],
    'x-fmt/15' => [
        'name' => 'MS-DOS Text File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/16' => [
        'name' => 'Unicode Text File',
        'other_name' => ' ',
        'category' => 'Text (Unstructured)',
        'callbacks' => [
        ],
    ],
    'x-fmt/17' => [
        'name' => 'Microsoft Excel Template',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'x-fmt/18' => [
        'name' => 'Comma Separated Values',
        'other_name' => ' ',
        'category' => 'Dataset',
        'callbacks' => [
        ],
    ],
    'x-fmt/19' => [
        'name' => '3D Studio',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'x-fmt/20' => [
        'name' => 'Adobe Illustrator',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/21' => [
        'name' => '7-bit ANSI Text',
        'other_name' => ' ',
        'category' => 'Text (Unstructured)',
        'callbacks' => [
        ],
    ],
    'x-fmt/22' => [
        'name' => '7-bit ASCII Text',
        'other_name' => ' ',
        'category' => 'Text (Unstructured)',
        'callbacks' => [
        ],
    ],
    'x-fmt/23' => [
        'name' => 'Microsoft Excel Backup',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'x-fmt/24' => [
        'name' => 'AutoCAD Block Attribute Template',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/25' => [
        'name' => 'OS/2 Bitmap',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/26' => [
        'name' => 'AutoCAD Batch Plot File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/27' => [
        'name' => 'AutoCAD Batch Plot File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/28' => [
        'name' => 'CALS Compressed Bitmap',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/29' => [
        'name' => 'CorelDraw Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/30' => [
        'name' => 'CorelDraw Template',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/31' => [
        'name' => 'CorelDraw Compressed Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/32' => [
        'name' => 'Harvard Graphics Chart',
        'other_name' => ' ',
        'category' => 'Image (Vector), Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
            1 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'x-fmt/33' => [
        'name' => 'Corel R.A.V.E.',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/34' => [
        'name' => 'Corel Presentation Exchange File',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/35' => [
        'name' => 'Corel Presentation Exchange File',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/36' => [
        'name' => 'Corel CMX Compressed',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/37' => [
        'name' => 'AutoCAD Colour-Dependant Plot Style Table',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/38' => [
        'name' => 'AutoCAD Custom Dictionary',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/39' => [
        'name' => 'AutoCAD dbConnect Query Set',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/40' => [
        'name' => 'AutoCAD dbConnect Template Set',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/41' => [
        'name' => 'Data Interchange Format',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'x-fmt/42' => [
        'name' => 'Wordperfect Secondary File',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/43' => [
        'name' => 'Wordperfect Secondary File',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/44' => [
        'name' => 'WordPerfect for MS-DOS/Windows Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/45' => [
        'name' => 'Microsoft Word Document Template',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/46' => [
        'name' => 'Microsoft Excel ODBC Query',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'x-fmt/47' => [
        'name' => 'Micrografx Draw',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/48' => [
        'name' => 'Visual Basic Macro',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/49' => [
        'name' => 'AutoCAD Design Web Format',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'x-fmt/50' => [
        'name' => 'AutoCAD Drawing Standards File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/51' => [
        'name' => 'AutoCAD Drawing Template',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/52' => [
        'name' => 'Drawing Interchange Format Style Extract',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/53' => [
        'name' => 'Macromedia Freehand',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/54' => [
        'name' => 'AutoCAD Font Mapping Table',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/55' => [
        'name' => 'Frame Vector Metafile',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/56' => [
        'name' => 'Kodak FlashPix Image',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/57' => [
        'name' => 'Ventura Publisher Vector Graphics',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/58' => [
        'name' => 'Microsoft Excel Web Query',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'x-fmt/59' => [
        'name' => 'AutoCAD Last Saved Layer State',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/60' => [
        'name' => 'AutoCAD Linetype Definition File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/61' => [
        'name' => 'AutoCAD Landscape Library',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/62' => [
        'name' => 'Log File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/63' => [
        'name' => 'AutoLISP File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/64' => [
        'name' => 'Microsoft Word for Macintosh Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/65' => [
        'name' => 'Microsoft Word for Macintosh Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/66' => [
        'name' => 'Microsoft Access Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/67' => [
        'name' => 'Met Metafile',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/68' => [
        'name' => 'AutoCAD Compiled Menu',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/69' => [
        'name' => 'AutoLISP Menu Source File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/70' => [
        'name' => 'AutoCAD Menu Resource File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/71' => [
        'name' => 'AutoCAD Source Menu File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/72' => [
        'name' => 'AutoCAD Template Menu File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/73' => [
        'name' => 'Microsoft Outlook Address Book',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/74' => [
        'name' => 'Microsoft Excel OLAP Query',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'x-fmt/75' => [
        'name' => 'Microsoft Outlook Personal Address Book',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/76' => [
        'name' => 'CorelDraw Pattern',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/77' => [
        'name' => 'AutoCAD Plot Configuration File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/78' => [
        'name' => 'AutoCAD Plot Configuration File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/79' => [
        'name' => 'AutoCAD Plot Configuration File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/80' => [
        'name' => 'Macintosh PICT Image',
        'other_name' => ' ',
        'category' => 'Image (Raster), Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/81' => [
        'name' => 'Inkwriter/Notetaker Template',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/82' => [
        'name' => 'Lotus 1-2-3 Chart',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/83' => [
        'name' => 'Hewlett Packard Vector Graphic Plotter File',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/84' => [
        'name' => 'Microsoft Powerpoint Design Template',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'x-fmt/85' => [
        'name' => 'Picture Publisher Bitmap',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/86' => [
        'name' => 'Microsoft Powerpoint Add-In',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/87' => [
        'name' => 'Microsoft Powerpoint Presentation Show',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPowerpoint',
        ],
    ],
    'x-fmt/88' => [
        'name' => 'Microsoft Powerpoint Presentation',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPowerpoint',
        ],
    ],
    'x-fmt/89' => [
        'name' => 'Freelance File',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'x-fmt/90' => [
        'name' => 'Microsoft Print File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/91' => [
        'name' => 'Postscript',
        'other_name' => ' ',
        'category' => 'Page Description',
        'callbacks' => [
        ],
    ],
    'x-fmt/92' => [
        'name' => 'Adobe Photoshop',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/93' => [
        'name' => 'Postscript Support File',
        'other_name' => ' ',
        'category' => 'Page Description',
        'callbacks' => [
        ],
    ],
    'x-fmt/94' => [
        'name' => 'Pocket Word Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/95' => [
        'name' => 'Inkwriter/Notetaker Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/96' => [
        'name' => 'Pocket Word Template',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/97' => [
        'name' => 'Microsoft Excel OLE DB Query',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'x-fmt/98' => [
        'name' => 'AutoCAD ACIS Export File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/99' => [
        'name' => 'Schedule+ Contacts',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/100' => [
        'name' => 'AutoCAD Script',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/101' => [
        'name' => 'Harvard Graphics Show',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'x-fmt/102' => [
        'name' => '3D Studio Shapes',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'x-fmt/103' => [
        'name' => 'AutoCAD Compiled Shape/Font File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/104' => [
        'name' => 'AutoCAD Slide Library',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/105' => [
        'name' => 'AutoCAD Slide',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/106' => [
        'name' => 'Microsoft Symbolic Link (SYLK) File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/107' => [
        'name' => 'AutoCAD Named Plot Style Table',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/108' => [
        'name' => 'STL (Standard Tessellation Language) ASCII',
        'other_name' => 'Stereolithography File (ASCII)',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'x-fmt/109' => [
        'name' => 'Scalable Vector Graphics Compressed',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/110' => [
        'name' => 'Fixed Width Values Text File',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'x-fmt/111' => [
        'name' => 'Plain Text File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/112' => [
        'name' => 'AutoCAD External Database Configuration File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/113' => [
        'name' => 'Microsoft Visio Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/114' => [
        'name' => 'Lotus 1-2-3 Worksheet',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'x-fmt/115' => [
        'name' => 'Lotus 1-2-3 Worksheet',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'x-fmt/116' => [
        'name' => 'Lotus 1-2-3 Worksheet',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'x-fmt/117' => [
        'name' => 'Lotus 1-2-3 Worksheet',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'x-fmt/118' => [
        'name' => 'Microsoft Works Spreadsheet',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'x-fmt/119' => [
        'name' => 'Windows Metafile Image',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/120' => [
        'name' => 'Microsoft Works for Windows',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/121' => [
        'name' => 'Quattro Pro Spreadsheet for DOS',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'x-fmt/122' => [
        'name' => 'Quattro Pro Spreadsheet for DOS',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'x-fmt/123' => [
        'name' => 'Microsoft Excel Macro',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'x-fmt/124' => [
        'name' => 'Microsoft Excel Add-In',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'x-fmt/125' => [
        'name' => 'Microsoft Excel Toolbar',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'x-fmt/126' => [
        'name' => 'Microsoft Excel Chart',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'x-fmt/127' => [
        'name' => 'AutoCAD Xref Log',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/128' => [
        'name' => 'Microsoft Excel Workspace',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkExcel',
            1 => 'FileValidator\Utility\FileValidator::checkXlsx',
        ],
    ],
    'x-fmt/129' => [
        'name' => 'Microsoft Word for Macintosh Document',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/130' => [
        'name' => 'MS-DOS Text File with line breaks',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/131' => [
        'name' => 'Stationery for Mac OS X',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/132' => [
        'name' => 'Speller Custom Dictionary',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/133' => [
        'name' => 'Speller Exclude Dictionary',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/134' => [
        'name' => 'AutoCAD Device-Independent Binary Plotter File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/135' => [
        'name' => 'Audio Interchange File Format',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'x-fmt/136' => [
        'name' => 'Audio Interchange File Format (compressed)',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'x-fmt/137' => [
        'name' => 'Electronic Arts Music',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'x-fmt/138' => [
        'name' => 'Active Server Page',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'x-fmt/139' => [
        'name' => 'NeXT/Sun sound',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'x-fmt/140' => [
        'name' => 'Silicon Graphics Image',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/141' => [
        'name' => 'Calendar Creator Plus Data File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/142' => [
        'name' => 'Computer Graphics Metafile ASCII',
        'other_name' => ' ',
        'category' => 'Image (Raster), Image (Vector), Text (Structured)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/143' => [
        'name' => 'OS/2 Change Control File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/144' => [
        'name' => 'Corel Photo-Paint Image',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/145' => [
        'name' => 'Stats+ Data File',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'x-fmt/146' => [
        'name' => 'Scitex Continuous Tone Bitmap',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/147' => [
        'name' => 'Paradox Database Table',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/148' => [
        'name' => 'IBM DisplayWrite DCA Text File',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/149' => [
        'name' => 'Desktop Color Separation File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/150' => [
        'name' => 'Visual FoxPro Database Container File',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/151' => [
        'name' => 'Micrografx Designer',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/152' => [
        'name' => 'Digital Video',
        'other_name' => ' ',
        'category' => 'Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'x-fmt/153' => [
        'name' => 'Microsoft Windows Enhanced Metafile',
        'other_name' => ' ',
        'category' => 'Image (Raster), Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/154' => [
        'name' => 'AutoDesk FLIC Animation',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/155' => [
        'name' => 'AutoCAD Film Roll',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/156' => [
        'name' => 'Ventura Publisher',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/157' => [
        'name' => 'Interchange File',
        'other_name' => ' ',
        'category' => 'Aggregate',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'x-fmt/158' => [
        'name' => 'Initial Graphics Exchange Specification (IGES)',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'x-fmt/159' => [
        'name' => 'GEM Image',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/160' => [
        'name' => 'Java Servlet Page',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/161' => [
        'name' => 'MacPaint Image',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/162' => [
        'name' => 'Adobe FrameMaker Interchange Format',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'x-fmt/163' => [
        'name' => 'NAP Metafile',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/164' => [
        'name' => 'Portable Bitmap Image - ASCII',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/165' => [
        'name' => 'Kodak PhotoCD Image',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/166' => [
        'name' => 'PICS Animation',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/167' => [
        'name' => 'Adobe PhotoDeluxe',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/168' => [
        'name' => 'Broderbund Print Shop Deluxe',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'x-fmt/169' => [
        'name' => 'PHP Script Page',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/170' => [
        'name' => 'PC Paint Bitmap',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/171' => [
        'name' => 'Inset Systems Bitmap',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/172' => [
        'name' => 'Microsoft FoxPro Library',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/173' => [
        'name' => 'PageMaker Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/174' => [
        'name' => 'PageMaker Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/175' => [
        'name' => 'MacPaint Graphics',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/176' => [
        'name' => 'Picture Publisher Bitmap',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/177' => [
        'name' => 'Microsoft PowerPoint Graphics File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/178' => [
        'name' => 'Portable Pixel Map - ASCII',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/179' => [
        'name' => 'Microsoft Visual Modeller Petal file (ASCII)',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/180' => [
        'name' => 'Instalit Script',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/181' => [
        'name' => 'PageMaker Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/182' => [
        'name' => 'Quark Xpress Data File',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'x-fmt/183' => [
        'name' => 'RealAudio Metafile',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'x-fmt/184' => [
        'name' => 'Sun Raster Image',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/185' => [
        'name' => 'Raw Bitmap',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/186' => [
        'name' => 'Silicon Graphics RGB File',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/187' => [
        'name' => 'Fractal Design Painter RIFF Bitmap Graphics',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/188' => [
        'name' => 'SDSC Image Tool Wavefront Raster Image',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/189' => [
        'name' => 'SDSC Image Tool Run-Length Encoded Bitmap',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/190' => [
        'name' => 'RealMedia',
        'other_name' => ' ',
        'category' => 'Audio, Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
            1 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'x-fmt/191' => [
        'name' => 'AMI Professional Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/192' => [
        'name' => 'SAS for MS-DOS Catalog',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/193' => [
        'name' => 'Unisys (Sperry) System Data File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/194' => [
        'name' => 'IRIS Graphics',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/195' => [
        'name' => 'Standard Generalized Markup Language',
        'other_name' => 'SGML',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'x-fmt/196' => [
        'name' => 'NeXt Sound',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'x-fmt/197' => [
        'name' => 'DataFlex Query Tag Name',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/198' => [
        'name' => 'Pagemaker TableEditor Graphics',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/199' => [
        'name' => 'Turbo Debugger Keystroke Recording File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/200' => [
        'name' => 'PageMaker Time Stamp File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/201' => [
        'name' => 'CCITT G.711 Audio',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'x-fmt/202' => [
        'name' => 'Corel Wavelet Compressed Bitmap',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/203' => [
        'name' => 'WordPerfect for Windows Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/204' => [
        'name' => 'Microsoft Word for Windows Macro',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/205' => [
        'name' => 'WordStar for MS-DOS Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/206' => [
        'name' => 'WordStar for Windows Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/207' => [
        'name' => 'X-Windows Bitmap Image',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/208' => [
        'name' => 'X-Windows Pixmap Image',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/209' => [
        'name' => 'SDSC Image Tool X Window Dump Format',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/210' => [
        'name' => 'XYWrite Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/211' => [
        'name' => 'XYWrite Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/212' => [
        'name' => 'Lotus 1-2-3 Worksheet',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/213' => [
        'name' => 'Quicken Data File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/214' => [
        'name' => 'Microsoft Paint',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/215' => [
        'name' => 'GEM Metafile Format',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/216' => [
        'name' => 'Microsoft Powerpoint Packaged Presentation',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'x-fmt/217' => [
        'name' => 'Adobe ACD',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'x-fmt/218' => [
        'name' => 'ESRI Arc/Info Binary Grid',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/219' => [
        'name' => 'Internet Archive',
        'other_name' => ' ',
        'category' => 'Aggregate',
        'callbacks' => [
        ],
    ],
    'x-fmt/220' => [
        'name' => 'Applixware Spreadsheet',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'x-fmt/221' => [
        'name' => 'MapBrowser/MapWriter Vector Map Data',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/222' => [
        'name' => 'CD Audio',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'x-fmt/223' => [
        'name' => 'Autodesk Animator CEL File Format',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/224' => [
        'name' => 'Cascading Style Sheet',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'x-fmt/225' => [
        'name' => 'ESRI MapInfo Data File',
        'other_name' => ' ',
        'category' => 'GIS',
        'callbacks' => [
        ],
    ],
    'x-fmt/226' => [
        'name' => 'ESRI Arc/Info Export File',
        'other_name' => ' ',
        'category' => 'GIS',
        'callbacks' => [
        ],
    ],
    'x-fmt/227' => [
        'name' => 'Geography Markup Language',
        'other_name' => 'GML',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'x-fmt/228' => [
        'name' => 'Applixware Bitmap',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/229' => [
        'name' => 'Intergraph Raster Image',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/230' => [
        'name' => 'MIDI Audio',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkMidi',
        ],
    ],
    'x-fmt/231' => [
        'name' => 'ESRI MapInfo Export File',
        'other_name' => ' ',
        'category' => 'GIS',
        'callbacks' => [
        ],
    ],
    'x-fmt/232' => [
        'name' => 'Microsoft Project Export File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/233' => [
        'name' => 'Paint Shop Pro Image',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/234' => [
        'name' => 'Paint Shop Pro Image',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/235' => [
        'name' => 'ESRI Arc/View ShapeFile',
        'other_name' => ' ',
        'category' => 'GIS',
        'callbacks' => [
        ],
    ],
    'x-fmt/236' => [
        'name' => 'WordStar for MS-DOS Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/237' => [
        'name' => 'WordStar for MS-DOS Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/238' => [
        'name' => 'Microsoft Access Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/239' => [
        'name' => 'Microsoft Access Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/240' => [
        'name' => 'Microsoft Access Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/241' => [
        'name' => 'Microsoft Access Database',
        'other_name' => 'Microsoft Access Database (XP)',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/242' => [
        'name' => 'Microsoft FoxPro Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/243' => [
        'name' => 'Microsoft Project',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/244' => [
        'name' => 'Microsoft Project',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/245' => [
        'name' => 'Microsoft Project',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/246' => [
        'name' => 'Microsoft Project',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/247' => [
        'name' => 'Microsoft Project',
        'other_name' => 'Microsoft Project file (XP)',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/248' => [
        'name' => 'Microsoft Outlook Personal Folders (ANSI)',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/249' => [
        'name' => 'Microsoft Outlook Personal Folders (Unicode)',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/250' => [
        'name' => 'Microsoft Outlook Personal Folders',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/251' => [
        'name' => 'Microsoft Outlook Personal Folders',
        'other_name' => 'Microsoft Outlook Personal Folders (XP)',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/252' => [
        'name' => 'Microsoft Publisher',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'x-fmt/253' => [
        'name' => 'Microsoft Publisher',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'x-fmt/254' => [
        'name' => 'Microsoft Publisher',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'x-fmt/255' => [
        'name' => 'Microsoft Publisher',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'x-fmt/256' => [
        'name' => 'Microsoft Publisher',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'x-fmt/257' => [
        'name' => 'Microsoft Publisher',
        'other_name' => 'Microsoft Publisher (XP)',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'x-fmt/258' => [
        'name' => 'Microsoft Visio Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/259' => [
        'name' => 'Microsoft Visio Drawing',
        'other_name' => 'Microsoft Visio Drawing (XP)',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/260' => [
        'name' => 'WordStar for MS-DOS Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/261' => [
        'name' => 'WordStar for MS-DOS Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/262' => [
        'name' => 'WordStar for Windows Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/263' => [
        'name' => 'ZIP Format',
        'other_name' => ' ',
        'category' => 'Aggregate',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkZip',
        ],
    ],
    'x-fmt/264' => [
        'name' => 'RAR Archive',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkRar',
        ],
    ],
    'x-fmt/265' => [
        'name' => 'Tape Archive Format',
        'other_name' => 'tar',
        'category' => 'Aggregate',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkTar',
        ],
    ],
    'x-fmt/266' => [
        'name' => 'GZIP Format',
        'other_name' => ' ',
        'category' => 'Aggregate',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkGzip',
        ],
    ],
    'x-fmt/267' => [
        'name' => 'BZIP Compressed Archive',
        'other_name' => ' ',
        'category' => 'Aggregate',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkBzip',
        ],
    ],
    'x-fmt/268' => [
        'name' => 'BZIP2 Compressed Archive',
        'other_name' => ' ',
        'category' => 'Aggregate',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkBzip',
        ],
    ],
    'x-fmt/269' => [
        'name' => 'ZOO Compressed Archive',
        'other_name' => ' ',
        'category' => 'Aggregate',
        'callbacks' => [
        ],
    ],
    'x-fmt/270' => [
        'name' => 'OS/2 Bitmap',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/271' => [
        'name' => 'dBASE Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/272' => [
        'name' => 'dBASE Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/273' => [
        'name' => 'Microsoft Word for MS-DOS Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/274' => [
        'name' => 'Microsoft Word for MS-DOS Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/275' => [
        'name' => 'Microsoft Word for MS-DOS Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/276' => [
        'name' => 'Microsoft Word for MS-DOS Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/277' => [
        'name' => 'Real Video',
        'other_name' => ' ',
        'category' => 'Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'x-fmt/278' => [
        'name' => 'RealAudio',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'x-fmt/279' => [
        'name' => 'MPEG 1/2 Audio Layer 3 Streaming',
        'other_name' => 'MP3',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'x-fmt/280' => [
        'name' => 'XML Schema Definition',
        'other_name' => ' ',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkXsd',
        ],
    ],
    'x-fmt/281' => [
        'name' => 'Extensible Stylesheet Language',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/282' => [
        'name' => '8-bit ANSI Text',
        'other_name' => ' ',
        'category' => 'Text (Unstructured)',
        'callbacks' => [
        ],
    ],
    'x-fmt/283' => [
        'name' => '8-bit ASCII Text',
        'other_name' => ' ',
        'category' => 'Text (Unstructured)',
        'callbacks' => [
        ],
    ],
    'x-fmt/284' => [
        'name' => 'IBM DisplayWrite Final Form Text File',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/285' => [
        'name' => 'IBM DisplayWrite Revisable Form Text File',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/286' => [
        'name' => 'DEC Data Exchange File',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'x-fmt/287' => [
        'name' => 'DEC WPS Plus Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/288' => [
        'name' => 'IBM DisplayWrite Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/289' => [
        'name' => 'IBM DisplayWrite Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/290' => [
        'name' => 'AMI Draw Vector Image',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/291' => [
        'name' => 'CorelDraw Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/292' => [
        'name' => 'CorelDraw Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/293' => [
        'name' => 'Hewlett Packard Graphics Language',
        'other_name' => 'HPGL',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/294' => [
        'name' => 'Micrografx Draw',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/295' => [
        'name' => 'Micrografx Draw',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/296' => [
        'name' => 'Micrografx Designer',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/297' => [
        'name' => 'Paint Shop Pro Image',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/298' => [
        'name' => 'Paint Shop Pro Image',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/299' => [
        'name' => 'X-Windows Bitmap Image',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/300' => [
        'name' => 'X-Windows Screen Dump File',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/301' => [
        'name' => 'ACBM Graphics',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/302' => [
        'name' => 'Adobe FrameMaker Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/303' => [
        'name' => 'Aldus Freehand Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/304' => [
        'name' => 'Aldus Freehand Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/305' => [
        'name' => 'Apple Sound',
        'other_name' => ' ',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'x-fmt/306' => [
        'name' => 'AutoSketch Drawing',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/307' => [
        'name' => 'Paradox Database Memo Field (Binary Large Object)',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/308' => [
        'name' => 'Btrieve Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/309' => [
        'name' => 'ChiWriter Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/310' => [
        'name' => 'Corel Chart',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/311' => [
        'name' => 'dBASE Text Memo',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'x-fmt/312' => [
        'name' => 'DesignCAD Drawing',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'x-fmt/313' => [
        'name' => 'DesignCAD for Windows Drawing',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'x-fmt/314' => [
        'name' => 'Digital Terrain Elevation Data',
        'other_name' => ' ',
        'category' => 'GIS',
        'callbacks' => [
        ],
    ],
    'x-fmt/315' => [
        'name' => 'Document Type Definition',
        'other_name' => ' ',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'x-fmt/316' => [
        'name' => 'Dr Halo Bitmap',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/317' => [
        'name' => 'ESRI Arc/View Project',
        'other_name' => ' ',
        'category' => 'GIS',
        'callbacks' => [
        ],
    ],
    'x-fmt/318' => [
        'name' => 'FileMaker Pro Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/319' => [
        'name' => 'FileMaker Pro Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/320' => [
        'name' => 'Fractal Image',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/321' => [
        'name' => 'Framework Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/322' => [
        'name' => 'Framework Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/323' => [
        'name' => 'Framework Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/324' => [
        'name' => 'Harvard Graphics Presentation',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'x-fmt/325' => [
        'name' => 'Harvard Graphics Vector Graphics',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/326' => [
        'name' => 'Hewlett Packard AdvanceWrite Text File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/327' => [
        'name' => 'IntelliDraw Vector Graphics',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/328' => [
        'name' => 'InterBase Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/329' => [
        'name' => 'Interleaf Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/330' => [
        'name' => 'JustWrite Text Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/331' => [
        'name' => 'Lotus 1-2-3 Spreadsheet Formatting File',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'x-fmt/332' => [
        'name' => 'Lotus 1-2-3 Spreadsheet Formatting File',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'x-fmt/333' => [
        'name' => 'Lotus Approach View File',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/334' => [
        'name' => 'Lotus Approach View File',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/335' => [
        'name' => 'Lotus Freelance Smartmaster Graphics',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'x-fmt/336' => [
        'name' => 'Lotus Notes Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/337' => [
        'name' => 'Lotus Notes Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/338' => [
        'name' => 'Lotus Notes Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/339' => [
        'name' => 'Lotus Notes File',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/340' => [
        'name' => 'Lotus WordPro Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/341' => [
        'name' => 'Macromedia Director',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'x-fmt/342' => [
        'name' => 'Microsoft FoxPro Memo',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'x-fmt/343' => [
        'name' => 'Microsoft Visual FoxPro Table',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/344' => [
        'name' => 'Microsoft Works Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/345' => [
        'name' => 'Microsoft Works Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/346' => [
        'name' => 'Microstation CAD Drawing',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/347' => [
        'name' => 'MultiMate Text File',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/348' => [
        'name' => 'Multipage Zsoft Paintbrush Bitmap Graphics',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/349' => [
        'name' => 'Nota Bene Text File',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/350' => [
        'name' => 'OmniPage Pro Document',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/351' => [
        'name' => 'PageMaker Document',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/352' => [
        'name' => 'PageMaker Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/353' => [
        'name' => 'Professional Write Text File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/354' => [
        'name' => 'SAP Document',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/355' => [
        'name' => 'SAS Data File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/356' => [
        'name' => 'SAS for MS-DOS Database',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/357' => [
        'name' => 'Scanstudio 16-Colour Bitmap',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/358' => [
        'name' => 'Silicon Graphics Graphics File',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/359' => [
        'name' => 'StarOffice Calc',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'x-fmt/360' => [
        'name' => 'StarOffice Impress',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'x-fmt/361' => [
        'name' => 'StatGraphics Data File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/362' => [
        'name' => 'StratGraphics Data File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/363' => [
        'name' => 'SuperCalc Spreadsheet',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'x-fmt/364' => [
        'name' => 'SuperCalc Spreadsheet',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'x-fmt/365' => [
        'name' => 'TeX Binary File',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'x-fmt/366' => [
        'name' => '',
        'other_name' => '',
        'category' => '',
        'callbacks' => [
        ],
    ],
    'x-fmt/367' => [
        'name' => 'Truevision TGA Bitmap',
        'other_name' => 'TGA, Targa Bitmap',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/368' => [
        'name' => 'VisiCalc Database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/369' => [
        'name' => 'Vista Pro Graphics',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/370' => [
        'name' => 'WordStar for MS-DOS Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/371' => [
        'name' => 'XYWrite for Windows Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/372' => [
        'name' => 'XYWrite Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/373' => [
        'name' => 'XYWrite Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/374' => [
        'name' => 'CorelDraw Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/375' => [
        'name' => 'CorelDraw Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/376' => [
        'name' => 'Paint Shop Pro Image',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/377' => [
        'name' => 'Paint Shop Pro Image',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/378' => [
        'name' => 'CorelDraw Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/379' => [
        'name' => 'CorelDraw Drawing',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/380' => [
        'name' => 'dBASE for Windows database',
        'other_name' => ' ',
        'category' => 'Database',
        'callbacks' => [
        ],
    ],
    'x-fmt/381' => [
        'name' => 'Dia Graphics Format',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/382' => [
        'name' => 'Macromedia FLV',
        'other_name' => ' ',
        'category' => 'Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'x-fmt/383' => [
        'name' => 'Flexible Image Transport System',
        'other_name' => 'FITS',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/384' => [
        'name' => 'Quicktime',
        'other_name' => 'MOV, QT',
        'category' => 'Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'x-fmt/385' => [
        'name' => 'MPEG-1 Program Stream',
        'other_name' => ' ',
        'category' => 'Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'x-fmt/386' => [
        'name' => 'MPEG-2 Program Stream',
        'other_name' => ' ',
        'category' => 'Video',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkVideo',
        ],
    ],
    'x-fmt/387' => [
        'name' => 'Exchangeable Image File Format (Uncompressed)',
        'other_name' => 'Exif Uncompressed Image (2.2)',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/388' => [
        'name' => 'Exchangeable Image File Format (Uncompressed)',
        'other_name' => 'Exif Uncompressed Image (2.1)',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/389' => [
        'name' => 'Exchangeable Image File Format (Audio)',
        'other_name' => 'Exif Audio (2.1)',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'x-fmt/390' => [
        'name' => 'Exchangeable Image File Format (Compressed)',
        'other_name' => 'Exif Compressed Image (2.1)',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/391' => [
        'name' => 'Exchangeable Image File Format (Compressed)',
        'other_name' => 'Exif Compressed Image (2.2)',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/392' => [
        'name' => 'JP2 (JPEG 2000 part 1)',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/393' => [
        'name' => 'WordPerfect for MS-DOS Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/394' => [
        'name' => 'WordPerfect for MS-DOS/Windows Document',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/395' => [
        'name' => 'WordPerfect Graphics Metafile',
        'other_name' => 'WPG (1.0)',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/396' => [
        'name' => 'Exchangeable Image File Format (Audio)',
        'other_name' => 'Exif Audio (2.2)',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'x-fmt/397' => [
        'name' => 'Exchangeable Image File Format (Audio)',
        'other_name' => 'Exif Audio (2.0)',
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'x-fmt/398' => [
        'name' => 'Exchangeable Image File Format (Compressed)',
        'other_name' => 'Exif Compressed Image (2.0)',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/399' => [
        'name' => 'Exchangeable Image File Format (Uncompressed)',
        'other_name' => 'Exif Uncompressed Image (2.0)',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/400' => [
        'name' => 'StarOffice Writer',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/401' => [
        'name' => 'StarOffice Draw',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/402' => [
        'name' => 'StarOffice Draw',
        'other_name' => ' ',
        'category' => 'Image (Vector)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/403' => [
        'name' => 'StarOffice Writer',
        'other_name' => ' ',
        'category' => 'Word Processor',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkWord',
        ],
    ],
    'x-fmt/404' => [
        'name' => 'StarOffice Calc',
        'other_name' => ' ',
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ],
    ],
    'x-fmt/405' => [
        'name' => 'StarOffice Impress',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'x-fmt/406' => [
        'name' => 'PostScript',
        'other_name' => ' ',
        'category' => 'Page Description',
        'callbacks' => [
        ],
    ],
    'x-fmt/407' => [
        'name' => 'PostScript',
        'other_name' => ' ',
        'category' => 'Page Description',
        'callbacks' => [
        ],
    ],
    'x-fmt/408' => [
        'name' => 'PostScript',
        'other_name' => ' ',
        'category' => 'Page Description',
        'callbacks' => [
        ],
    ],
    'x-fmt/409' => [
        'name' => 'MS-DOS Executable',
        'other_name' => 'EXE',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/410' => [
        'name' => 'Windows New Executable',
        'other_name' => 'EXE',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/411' => [
        'name' => 'Windows Portable Executable',
        'other_name' => 'Common Object File Format, COFF, EXE',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/412' => [
        'name' => 'Java Archive Format',
        'other_name' => 'JAR',
        'category' => 'Aggregate',
        'callbacks' => [
        ],
    ],
    'x-fmt/413' => [
        'name' => 'Batch file (executable)',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'x-fmt/414' => [
        'name' => 'Windows Cabinet File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/415' => [
        'name' => 'Java Compiled Object Code',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/416' => [
        'name' => 'BinHex Binary Text',
        'other_name' => ' ',
        'category' => 'Aggregate',
        'callbacks' => [
        ],
    ],
    'x-fmt/417' => [
        'name' => 'HTML Extension File',
        'other_name' => ' ',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'x-fmt/418' => [
        'name' => 'Icon file format',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/419' => [
        'name' => 'DVD data file and backup data file',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/420' => [
        'name' => 'Windows Setup File',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'x-fmt/421' => [
        'name' => 'Text Configuration file',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'x-fmt/422' => [
        'name' => 'Java language source code file',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/423' => [
        'name' => 'JavaScript file',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/424' => [
        'name' => 'Deluxe Paint bitmap',
        'other_name' => ' ',
        'category' => 'Image (Raster)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkImage',
        ],
    ],
    'x-fmt/425' => [
        'name' => 'Generic Library File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/426' => [
        'name' => 'License file',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/427' => [
        'name' => 'Acrobat Language definition file',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/428' => [
        'name' => 'Microsoft Windows Shortcut',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/429' => [
        'name' => 'Microsoft Web Archive',
        'other_name' => ' ',
        'category' => 'Aggregate',
        'callbacks' => [
        ],
    ],
    'x-fmt/430' => [
        'name' => 'Microsoft Outlook Email Message',
        'other_name' => ' ',
        'category' => 'Email',
        'callbacks' => [
        ],
    ],
    'x-fmt/431' => [
        'name' => '',
        'other_name' => '',
        'category' => '',
        'callbacks' => [
        ],
    ],
    'x-fmt/432' => [
        'name' => '3DM',
        'other_name' => 'openNURBS (4), Rhino 3D Model (4)',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'x-fmt/433' => [
        'name' => '3DM',
        'other_name' => 'Rhino 3D Model (1), openNURBS (1)',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'x-fmt/434' => [
        'name' => '3DM',
        'other_name' => 'Rhino 3D Model (2), openNURBS (2)',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'x-fmt/435' => [
        'name' => '3DM',
        'other_name' => 'Rhino 3D Model (3), openNURBS (3)',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'x-fmt/436' => [
        'name' => 'CATIA Model',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'x-fmt/437' => [
        'name' => 'CATIA Project',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/438' => [
        'name' => 'CATIA',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'x-fmt/439' => [
        'name' => 'CATIA Model (Part Description)',
        'other_name' => ' ',
        'category' => 'Model',
        'callbacks' => [
        ],
    ],
    'x-fmt/440' => [
        'name' => 'CATIA Product Description',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/441' => [
        'name' => 'AutoCAD Database File Locking Information',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/442' => [
        'name' => 'form*Z Project File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/443' => [
        'name' => 'Revit Family File',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/444' => [
        'name' => 'Revit Family Template',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/445' => [
        'name' => 'Revit Template',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/446' => [
        'name' => 'Revit External Group',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/447' => [
        'name' => 'Revit Project',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/448' => [
        'name' => 'Revit Workspace',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/449' => [
        'name' => 'Steel Detailing Neutral Format',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/450' => [
        'name' => 'Adobe InDesign Document',
        'other_name' => ' ',
        'category' => 'Presentation',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkPresentation',
        ],
    ],
    'x-fmt/451' => [
        'name' => 'SketchUp Document',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/452' => [
        'name' => 'SketchUp Document',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/453' => [
        'name' => 'TrueType Font',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'x-fmt/454' => [
        'name' => 'Microsoft Internet Shortcut',
        'other_name' => ' ',
        'category' => 'Text (Structured)',
        'callbacks' => [
        ],
    ],
    'x-fmt/455' => [
        'name' => 'AutoCAD Drawing',
        'other_name' => ' ',
        'category' => ' ',
        'callbacks' => [
        ],
    ],
    'ext/aac' => [
        'name' => 'Advanced Audio Coding',
        'other_name' => 'AAC',
        'mime' => [
            'audio/aac',
            'audio/aacp',
            'audio/3gpp',
            'audio/3gpp2',
            'audio/mp4',
            'audio/mp4a-latm',
            'audio/mpeg4-generic',
            'audio/x-hx-aac-adts',
        ],
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'ext/m4a' => [
        'name' => 'Advanced Audio Coding',
        'other_name' => 'AAC',
        'mime' => [
            'audio/aac',
            'audio/aacp',
            'audio/3gpp',
            'audio/3gpp2',
            'audio/mp4',
            'audio/mp4a-latm',
            'audio/mpeg4-generic',
            'audio/x-hx-aac-adts',
        ],
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'ext/mp4' => [
        'name' => 'Advanced Audio Coding',
        'other_name' => 'AAC',
        'mime' => [
            'audio/aac',
            'audio/aacp',
            'audio/3gpp',
            'audio/3gpp2',
            'audio/mp4',
            'audio/mp4a-latm',
            'audio/mpeg4-generic',
            'audio/x-hx-aac-adts',
        ],
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'ext/3gp' => [
        'name' => 'Advanced Audio Coding',
        'other_name' => 'AAC',
        'mime' => [
            'audio/aac',
            'audio/aacp',
            'audio/3gpp',
            'audio/3gpp2',
            'audio/mp4',
            'audio/mp4a-latm',
            'audio/mpeg4-generic',
            'audio/x-hx-aac-adts',
        ],
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'ext/m4b' => [
        'name' => 'Advanced Audio Coding',
        'other_name' => 'AAC',
        'mime' => [
            'audio/aac',
            'audio/aacp',
            'audio/3gpp',
            'audio/3gpp2',
            'audio/mp4',
            'audio/mp4a-latm',
            'audio/mpeg4-generic',
            'audio/x-hx-aac-adts',
        ],
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'ext/m4p' => [
        'name' => 'Advanced Audio Coding',
        'other_name' => 'AAC',
        'mime' => [
            'audio/aac',
            'audio/aacp',
            'audio/3gpp',
            'audio/3gpp2',
            'audio/mp4',
            'audio/mp4a-latm',
            'audio/mpeg4-generic',
            'audio/x-hx-aac-adts',
        ],
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'ext/m4r' => [
        'name' => 'Advanced Audio Coding',
        'other_name' => 'AAC',
        'mime' => [
            'audio/aac',
            'audio/aacp',
            'audio/3gpp',
            'audio/3gpp2',
            'audio/mp4',
            'audio/mp4a-latm',
            'audio/mpeg4-generic',
            'audio/x-hx-aac-adts',
        ],
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'ext/m4v' => [
        'name' => 'Advanced Audio Coding',
        'other_name' => 'AAC',
        'mime' => [
            'audio/aac',
            'audio/aacp',
            'audio/3gpp',
            'audio/3gpp2',
            'audio/mp4',
            'audio/mp4a-latm',
            'audio/mpeg4-generic',
            'audio/x-hx-aac-adts',
        ],
        'category' => 'Audio',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkAudio',
        ],
    ],
    'ext/voc' => [
        'name' => 'Creative Voice files',
        'other_name' => 'VOC',
        'mime' => [
            'audio/x-unknown',
        ],
        'category' => 'Audio',
        'callbacks' => [
        ],
    ],
    'ext/dat' => [
        'name' => 'Data File',
        'other_name' => 'DAT',
        'mime' => [
            'text/plain',
        ],
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'ext/xsd' => [
        'name' => 'XML Schema Definition',
        'other_name' => 'XSD',
        'mime' => [
            'text/plain',
        ],
        'category' => 'Text (Mark-up)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkXsd',
        ],
    ],
    'ext/html' => [
        'name' => 'HyperText Markup Language',
        'other_name' => 'HTML',
        'mime' => [
            'text/plain',
            'text/html',
        ],
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'ext/htm' => [
        'name' => 'HyperText Markup Language',
        'other_name' => 'HTM',
        'mime' => [
            'text/plain',
            'text/html',
        ],
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'ext/php' => [
        'name' => 'PHP: Hypertext Preprocessor',
        'other_name' => 'PHP',
        'mime' => [
            'text/plain',
            'text/html',
            'text/x-php',
        ],
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'ext/js' => [
        'name' => 'Javascript',
        'other_name' => 'JS',
        'mime' => [
            'text/plain',
        ],
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'ext/css' => [
        'name' => 'Cascading Style Sheets',
        'other_name' => 'CSS',
        'mime' => [
            'text/plain',
        ],
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
    'ext/json' => [
        'name' => 'JSON Data Interchange Format',
        'other_name' => 'JSON',
        'mime' => [
            'text/plain',
            'application/octet-stream',
            'application/json'
        ],
        'category' => 'Text (Mark-up)',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkJson',
        ],
    ],
    'ext/sql' => [
        'name' => 'Structured Query Language',
        'other_name' => 'SQL',
        'mime' => [
            'text/plain'
        ],
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ]
    ],
    'ext/csv' => [
        'name' => 'Comma Separated Values',
        'other_name' => 'CSV',
        'mime' => [
            'text/plain'
        ],
        'category' => 'Spreadsheet',
        'callbacks' => [
            0 => 'FileValidator\Utility\FileValidator::checkSpreadsheet',
        ]
    ],
    'mime/inode/x-empty' => [
        'name' => 'Empty File',
        'other_name' => 'empty',
        'category' => 'Text (Mark-up)',
        'callbacks' => [
        ],
    ],
];
