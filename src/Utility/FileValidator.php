<?php
/**
 * Validation d'un fichier.
 *
 * Permet de vérifier l'intégrité d'un fichier (image/video/xml ect...)
 * Si un callback existe pour une catégorie de fichier, une vérification a lieu
 * Détecte si un fichier est corrompu, illisible ou mal formaté.
 *
 * @category    File
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2016, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */

namespace FileValidator\Utility;

use DOMDocument;
use Exception;
use ReflectionClass;

/**
 * Permet la validation d'un fichier.
 *
 * Utiliser la méthode <b>FileValidator::check($filename)</b> pour obtenir un
 * boolean vrai si valide, faux si corrompu
 *
 * <b>FileValidator::$displayErrors</b> permet d'activer l'affichage de certaines
 * erreurs. Laisser à faux en production car il est normal d'avoir des erreurs
 * sur un fichier invalide.
 *
 * <b>FileValidator::$allowNonFatalError</b> mis à false permet de rendre la
 * validation moins permissive
 *
 * Tout les callbacks que composent cette classe ont un suffix "check"
 * en commun.
 * Il est possible de les modifier avec <b>FileValidator::$callbacks</b>
 * avec un des callbacks spéciaux : <b>isValid</b> ou <b>isError</b>
 * Ça permet notamment de ne pas faire la vérifications sur un type de fichier
 * en particulier.
 */
class FileValidator
{
    /**
     * Permet d'afficher des erreurs (utile pour debug).
     *
     * @var bool
     */
    public static $displayErrors = false;

    /**
     * Désigne ou pas un fichier comme étant en erreur selon la présence
     * d'erreurs anodines.
     *
     * @var bool Si vrai, les erreurs non fatale seront tolérées
     */
    public static $allowNonFatalError = true;

    /**
     * Configuration des validations par fmt
     *
     * @see config/validations.php
     * @var array
     */
    public static $config;

    /**
     * Vérifie qu'un fichier est valide.
     *
     * @param string $filename
     * @return boolean
     * @throws Exception
     */
    public static function check($filename)
    {
        $isValid = true;

        if (!file_exists($filename)) {
            static::error("File Not Found");
            return false;
        }

        $matches = static::getPuid($filename);

        if (!$matches) {
            static::error("No Puid Found");
            return false;
        }

        foreach ($matches as $match) {
            /*
             * On extrait l'extension et le mime du fichier
             */
            $ext = strtolower(
                pathinfo($filename, PATHINFO_EXTENSION)
            );
            $mime = mime_content_type($filename);

            /*
             * Si siegfried n'a pas réussi à identifier le fichier
             */
            if ($match['warning'] === 'match on extension only'
                || $match['id'] === 'UNKNOWN'
            ) {
                // On tente de l'indentifier par extension + mime
                $id = 'ext/'.$ext;
                $valid = isset(static::$config[$id])
                    && in_array($mime, static::$config[$id]['mime']);

                // Sinon par mime uniquement
                if (!$valid) {
                    $id = 'mime/'.$mime;
                    $valid = isset(static::$config[$id]);
                }

                // Sinon c'est considéré comme faux
                if (!$valid) {
                    static::error("This file is not valid");
                    return false;
                }

                /**
                 * Si siegfried a identifier autre chose que le pronom, on zap
                 */
            } elseif ($match['ns'] !== 'pronom'
                || !isset(static::$config[$match['id']])
            ) {
                continue;

            /**
             * Sinon on récupère le PUID
             */
            } else {
                $id = $match['id'];
            }

            /**
             * On lance chaque callbacks, si un seul renvoi vrai, on considère
             * le fichier comme valide
             */
            foreach (static::$config[$id]['callbacks'] as $callback) {
                try {
                    $isValid = call_user_func(
                        explode('::', $callback),
                        $filename
                    );
                } catch (Exception $e) {
                    static::error($e->getMessage());
                }

                if ($isValid) {
                    return true;
                }
            }
        }

        return $isValid;
    }

    /**
     * @var array Mémorise les erreurs
     */
    public static $errors = [];

    /**
     * Garde les erreurs en mémoire, et les affiches si besoin
     * @param string $error
     */
    protected static function error($error)
    {
        if (static::$displayErrors) {
            trigger_error($error);
        }
        static::$errors[] = $error;
    }

    /**
     * Donne le pronom d'un fichier
     *
     * @param string $filename
     * @return boolean|array false si erreur, array si réussite
     * @throws Exception si le module siegfried n'est pas installé
     */
    public static function getPuid($filename)
    {
        $stdrFile = tempnam(sys_get_temp_dir(), 'stdr.output');

        exec(
            sprintf("sf -json %s 2> %s", escapeshellarg($filename), $stdrFile),
            $output,
            $code
        );
        $stdr = trim(
            preg_replace(
                '/^(\[FILE].*)?\n\[ERROR] empty source/',
                '',
                file_get_contents($stdrFile)
            )
        );

        unlink($stdrFile);

        if ($code == 127) {
            $message = "Le module siegfried semble être inutilisable.\n"
                . 'Allez sur https://www.itforarchivists.com/siegfried '
                . 'pour l\'installation.';
            throw new Exception($message);
        }

        if (!empty($stdr)) {
            static::error($stdr);
            return false;
        }

        return json_decode($output[0], true)['files'][0]['matches'];
    }

    /**
     * Garde en mémoire la configuration
     *
     * @param array $config
     */
    public static function loadConfig(array $config)
    {
        static::$config = $config;
    }

    /**
     * Logique commune des bibliothèques phpoffice.
     *
     * Attention ! cas de boucle infini détectée sur fichiers Excel et Powerpoint
     * C'est la raison pour laquelle ceux-ci ont leurs propre méthodes
     *
     * @param string $filename  Chemin vers le fichier à tester
     * @param string $basePath  Chemin vers la library
     * @param string $type      ex: PhpSpreadsheet
     * @param string $interface Nom de classe d'interface du reader
     *
     * @return bool
     *
     * @throws Exception Si le chemin n'existe pas ou n'est pas lisible
     */
    protected static function officeCheck(
        $filename,
        $basePath,
        $type,
        $interface
    ) {
        if (!is_dir($basePath.$type.'/Reader')) {
            throw new Exception(
                'Le chemin '.$basePath.$type.'/Reader'
                ." n'existe pas !"
            );
        }

        $readers = glob($basePath.$type.'/Reader/*');

        $isValid = false;
        $readable = false;
        foreach ($readers as $readerPath) {
            if (!preg_match('/([\w]+)\.php$/', $readerPath, $match)) {
                continue;
            }

            $reader = 'PhpOffice\\'.$type.'\\Reader\\'.$match[1];
            $interfaces = class_implements($reader);

            if (!isset($interfaces['PhpOffice\\'.$type.'\\Reader\\'.$interface])
            ) {
                continue;
            }

            $reflection = new ReflectionClass($reader);
            if ($reflection->isAbstract()) {
                continue;
            }

            try {
                $class = new $reader();
                if (method_exists($class, 'canRead')) {
                    $readable = $class->canRead($filename);
                }
            } catch (Exception $e) {
                $readable = false;
                static::error($e->getMessage());
            }

            if ($readable) {
                $isValid = true;
                break;
            }
        }

        return $isValid;
    }

    /**
     * Est valide une action qui ne renvoi rien en stderr.
     *
     * @param string $filename
     * @param string $action méthode bash à
     *                               exécuter
     * @param null $module module bash à
     *                               installer
     * @param bool $expectedStderr Vrai si on s'attend à un message
     *                               d'erreur. Un message d'erreur ne
     *                               provoquera pas une invalidation
     *
     * @return bool
     */
    protected static function shellCheck(
        $filename,
        $action,
        $module = null,
        $expectedStderr = false
    ) {
        $isValid = true;

        try {
            $stderr = [];
            exec(
                sprintf(
                    '%s %s 2>&1 >/dev/null',
                    $action,
                    escapeshellarg($filename)
                ),
                $stderr,
                $code
            );

            if ($code == 127) {
                $message = "Le module $action semble être inutilisable. "
                    ."Essayez :\nsudo apt-get install ".($module ?: $action);
                throw new Exception($message);
            }

            if ($code != 0 || (!$expectedStderr && !empty($stderr))) {
                $isValid = false;
            }
        } catch (Exception $e) {
            $isValid = false;
            static::error($e->getMessage());
        }

        return $isValid;
    }

    /**
     * Callback
     * Renvoi systématiquement vrai.
     *
     * @return bool
     */
    public static function isValid()
    {
        return true;
    }

    /**
     * Callback
     * Renvoi systématiquement faux.
     *
     * @return bool
     */
    public static function isError()
    {
        return false;
    }

    /**
     * Callback
     * Vérifie qu'une image est valide.
     *
     * @param string $filename
     *
     * @return bool
     */
    public static function checkImage($filename)
    {
        return static::shellCheck($filename, 'identify', 'imagemagick', static::$allowNonFatalError);
    }

    /**
     * Utilise ffmpeg afin de contrôler la validité d'un fichier.
     *
     * @param string $filename
     *
     * @return bool
     *
     * @throws Exception Si le module ffmpeg n'est pas installé
     */
    protected static function ffmpeg($filename)
    {
        exec(
            'ffmpeg -v error -i '.escapeshellarg($filename).' -f null /dev/null 2>&1 >/dev/null',
            $output,
            $code
        );

        if ($code === 127) {
            $message = "Le module ffmpeg semble être inutilisable. Essayez :\n"
                .'sudo apt-get install ffmpeg';
            throw new Exception($message);
        }

        return trim(implode('', $output)) === '';
    }

    /**
     * Utilise mediainfo afin de contrôler la validité d'un fichier.
     *
     * @param string $filename
     * @param string $arg
     *
     * @return bool
     *
     * @throws Exception Si le module mediainfo n'est pas installé
     */
    protected static function mediainfo($filename, $arg)
    {
        exec(
            'mediainfo --Inform="'.$arg.'" '.escapeshellarg($filename),
            $output,
            $code
        );

        if ($code == 127) {
            $message = "Le module mediainfo semble être inutilisable. Essayez :\n"
                .'sudo apt-get install mediainfo';
            throw new Exception($message);
        }

        return trim(implode('', $output)) !== '';
    }

    /**
     * Callback
     * Vérifie qu'une video est valide.
     *
     * @param string $filename
     *
     * @return bool
     * @throws Exception
     */
    public static function checkVideo($filename)
    {
        return static::ffmpeg($filename);
    }

    /**
     * Callback
     * Vérifie qu'un fichier audio est valide.
     *
     * @param string $filename
     *
     * @return bool
     * @throws Exception
     */
    public static function checkAudio($filename)
    {
        return static::ffmpeg($filename);
    }

    /**
     * Callback
     * Vérifie qu'un fichier midi est valide.
     *
     * @param string $filename
     *
     * @return bool
     * @throws Exception
     */
    public static function checkMidi($filename)
    {
        return static::mediainfo($filename, 'Audio;%Format%');
    }

    /**
     * Callback
     * Vérifie qu'un fichier presentation est valide.
     *
     * @param string $filename
     *
     * @return bool
     * @throws Exception
     */
    public static function checkPresentation($filename)
    {
        return static::officeCheck(
            $filename,
            ROOT_FILEVALIDATOR.'/vendor/phpoffice/phppresentation/src/',
            'PhpPresentation',
            'ReaderInterface'
        );
    }

    /**
     * Callback
     * Vérifie qu'un fichier calc est valide.
     *
     * @param string $filename
     *
     * @return bool
     * @throws Exception
     */
    public static function checkSpreadsheet($filename)
    {
        return static::officeCheck(
            $filename,
            ROOT_FILEVALIDATOR.'/vendor/phpoffice/phpspreadsheet/src/',
            'PhpSpreadsheet',
            'IReader'
        );
    }

    /**
     * Callback
     * Vérifie qu'un fichier word est valide.
     *
     * @param string $filename
     *
     * @return bool
     * @throws Exception
     */
    public static function checkWord($filename)
    {
        return static::officeCheck(
            $filename,
            ROOT_FILEVALIDATOR.'/vendor/phpoffice/phpword/src/',
            'PhpWord',
            'ReaderInterface'
        );
    }

    /**
     * Callback
     * Vérifie qu'un fichier pdf est valide.
     *
     * @see static::$allowNonFatalError
     *                          mettre a vrai pour éviter les faux positifs
     *                          cependant, la detection des mauvais pdf sera
     *                          moins efficasse
     *
     * @param string $filename
     *
     * @return bool
     */
    public static function checkPdf($filename)
    {
        return static::shellCheck(
            $filename,
            'pdfinfo',
            'poppler-utils',
            static::$allowNonFatalError
        );
    }

    /**
     * Callback
     * Vérifie qu'un fichier excel est valide.
     *
     * @param string $filename
     *
     * @return bool
     */
    public static function checkExcel($filename)
    {
        return self::shellCheck($filename, 'xls2csv', 'catdoc');
    }

    /**
     * Callback
     * Vérifie qu'un fichier powerpoint est valide.
     *
     * @param string $filename
     *
     * @return bool
     */
    public static function checkPowerpoint($filename)
    {
        return self::shellCheck($filename, 'catppt', 'catdoc');
    }

    /**
     * Callback
     * Vérifie qu'un fichier zip est valide.
     *
     * @param string $filename
     *
     * @return bool
     */
    public static function checkZip($filename)
    {
        return static::shellCheck($filename, 'zip -T', 'zip');
    }

    /**
     * Callback
     * Vérifie qu'un fichier gz est valide.
     *
     * @param string $filename
     *
     * @return bool
     */
    public static function checkGzip($filename)
    {
        return static::shellCheck($filename, 'gzip -t', 'gzip');
    }

    /**
     * Callback
     * Vérifie qu'un fichier bz est valide.
     *
     * @param string $filename
     *
     * @return bool
     */
    public static function checkBzip($filename)
    {
        return static::shellCheck($filename, 'bzip2 -t', 'bzip2');
    }

    /**
     * Callback
     * Vérifie qu'un fichier tar est valide.
     *
     * @param string $filename
     *
     * @return bool
     */
    public static function checkTar($filename)
    {
        return static::shellCheck(
            $filename,
            'tar -tvvf',
            'gzip',
            static::$allowNonFatalError
        );
    }

    /**
     * Callback
     * Vérifie qu'un fichier rar est valide.
     *
     * @param string $filename
     *
     * @return bool
     */
    public static function checkRar($filename)
    {
        return static::shellCheck($filename, 'rar t', 'rar');
    }

    /**
     * Callback
     * Vérifie qu'un fichier xml est valide.
     *
     * @param string $filename
     *
     * @return bool
     */
    public static function checkXml($filename)
    {
        return static::shellCheck(
            $filename,
            'xmllint --format',
            'libxml2-utils'
        );
    }

    /**
     * Callback
     * Vérifie qu'un fichier xlsx est valide.
     *
     * @param string $filename
     *
     * @return bool
     */
    public static function checkXlsx($filename)
    {
        return static::shellCheck(
            $filename,
            'xlsx2csv',
            'xlsx2csv'
        );
    }

    /**
     * Callback
     * Vérifie qu'un fichier json est valide.
     *
     * @param string $filename
     *
     * @return bool
     */
    public static function checkJson($filename)
    {
        $content = file_get_contents($filename);
        return json_decode($content, true) !== null;
    }

    /**
     * Callback
     * Vérifie qu'un XSD est valide.
     *
     * @param string $filename
     *
     * @return bool
     */
    public static function checkXsd($filename)
    {
        try {
            $dom = new DOMDocument;
            @$dom->load($filename);
            $ds = DIRECTORY_SEPARATOR;
            return @$dom->schemaValidate(ROOT_FILEVALIDATOR.$ds.'src'.$ds.'Lib'.$ds.'XMLSchema.xsd');
        } catch (Exception $e) {
        }
        return false;
    }
}
