<?php
/**
 * Corruption d'un fichier.
 *
 * Permet d'altérer un fichier afin de l'endommager de façon plus ou moins
 * permanente
 *
 * @category    File
 *
 * @author      Libriciel SCOP <contact@libriciel.coop>
 * @copyright   (c) 2016, Libriciel
 * @license     https://www.gnu.org/licenses/agpl-3.0.txt
 */

namespace FileValidator\Utility;

/**
 * Permet l'altération d'un fichier.
 *
 * Utiliser <b>FileCorruptor::lowLevelCorruption($filename)</b> ou
 * <b>FileCorruptor::highLevelCorruption($filename)</b> selon le degrés désiré
 * de dégradation du fichier.
 * Renvoi les données sous format binaire prêt à écrire avec un
 * <b>fwrite($filename, $binary)</b>
 */
class FileCorruptor
{
    /**
     * Renvoi le fichier légèrement altéré.
     *
     * @param string $filename
     *
     * @return string
     */
    public static function lowLevelCorruption($filename)
    {
        $data = bin2hex(file_get_contents($filename));

        if (strlen($data) < 2 * 8 + 2 * 5) {
            // On remplace le premier et dernier bytes par 00
            $altered = '00'.substr($data, 2, strlen($data) - 2).'00';
        } else {
            // Retire 5 bytes apres les 8 premiers bytes et en ajoute 5 à la fin
            $altered = substr($data, 0, 2 * 8).substr($data, 2 * 8 + 2 * 5).'0000000000';
        }

        return strlen($altered) === 0 ? '' : hex2bin($altered);
    }

    /**
     * Renvoi le fichier profondément modifié en conservant le même mime.
     *
     * @param string $filename
     *
     * @return string
     */
    public static function highLevelCorruption($filename)
    {
        $data = bin2hex(file_get_contents($filename));

        if (strlen($data) < 2 * 8 + 2 * 5) {
            // On remplace le premier et dernier bytes par 00
            $altered = '00'.substr($data, 2, strlen($data) - 2).'00';
        } else {
            // Ajoute 1 sur chaque bytes après le 8e (préserve le mime uniquement)
            $altered = substr($data, 0, 2 * 8);

            $length = strlen($data);
            for ($i = 16; $i < $length; $i += 2) {
                $bytes = $data[$i].$data[$i + 1];

                if (strtoupper($bytes) === 'FF') {
                    $bytes = '00';
                } else {
                    $bytes = dechex(hexdec($bytes) + 1);
                    if (strlen($bytes) === 1) {
                        $bytes = '0'.$bytes;
                    }
                }

                $altered .= $bytes;
            }
        }

        return strlen($altered) === 0 ? '' : hex2bin($altered);
    }
}
