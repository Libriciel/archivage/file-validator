# File Corruption Validator
[![build status](https://gitlab.libriciel.fr/file-validator/file-validator/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/file-validator/file-validator/pipelines/latest)
[![coverage report](https://gitlab.libriciel.fr/file-validator/file-validator/badges/master/coverage.svg)](https://asalae2.dev.libriciel.fr/coverage/file-validator/index.html)

## Prérequis

Pour utiliser convenablement cet outil, une installation Ubuntu 16.04 LTS est conseillée.
Une version PHP >= 5.6 est requise (compatible php 7).
Vous devrez en outre vous assurer que tout les packets suivant sont installés pour un fonctionnement optimal sur tous les types de fichiers supportés.
```bash
sudo apt-get install imagemagick mediainfo poppler-utils catdoc zip gzip rar webp libxml2-utils
sudo sed -i.orig -e's#<policy domain="coder" rights="none" pattern="PDF" />#<policy domain="coder" rights="read" pattern="PDF" />#' "/etc/ImageMagick-6/policy.xml"
```

## Installation

```bash
composer config repositories.libriciel/file-validator git git@gitlab.libriciel.fr:file-validator/file-validator.git
composer require libriciel/file-validator ~1.0
```

## Utilisation FileValidator

Un simple `require_once 'vendor/bootstrap.php';` vous donnera accès à la bibliothèque.
Ensuite, vous pouvez l'utiliser comme ceci : `FileValidator\Utility\FileValidator::check($filename);`.
Cette commande renvoie un boolean sur la validité du fichier.
Vous pouvez toujours aller voir dans la section __"test"__ pour un exemple d'utilisation.

Il est possible de désactiver une validation d'un type de fichier au besoin. Pour cela vous pouvez faire par exemple : `FileValidator\Utility\FileValidator::$callbacks['pdf'] = 'isValid';`, les pdf seront systématiquement considéré comme _valide_.
Vous pouvez utiliser cette particularité afin de surcharger la classe pour y ajouter un ou plusieurs callbacks. Si vous avez créé votre propre fonction __checkCustomPdf()__ vous pouvez l'utiliser en indiquant `FileValidator\Utility\FileValidator::$callbacks['pdf'] = 'checkCustomPdf';`.

Lors de la validation, il peut arriver de tomber sur des fichiers avec des erreurs de conception, mais qui reste tout de même lisible. Par défaut, le validateur les considérera comme valides, mais il peut arriver d'avoir besoin d'un contrôle plus stricte de la qualité d'un fichier. Pour cela, vous pouvez passer `FileValidator\Utility\FileValidator::$allowNonFatalError = false;`.

Si vous avez besoin de comprendre pourquoi un fichier est invalide, il est possible d'afficher certaines erreurs sous forme de _notice_ en passant ceci : `FileValidator\Utility\FileValidator::$displayErrors = true;`.

## Utilisation FileIdentify

Cette classe est utilisée par le FileValidator afin de choisir quel callback utiliser.
Vous pouvez être amené à la modifier (ses attributs ou par surcharge) afin d'ajouter / retirer des types de fichiers pris en charge.
Vous pouvez bien entendu l'utiliser pour identifier un type de fichier pour une autre utilisation.
Pour cela, il suffit d'utiliser `File\Identify\FileIdentify::identify($filename)` afin d’obtenir un type de fichier comme _'pdf'_ ou _'audio'_.

Il est possible d'ajouter une extension de fichier d'un type comme audio en ajoutant une valeur comme ceci : `File\Identify\FileIdentify::$extensionTypes['audio'][] = 'monext'`
Attention cela dit au type mime, si FileIdentify détecte une incohérence entre l'extension et le type mime, il reverra une erreur ! Il sera donc nécessaire dans certains cas de modifier la détection du type mime. C'est le cas notamment pour les mimes commençant par ___application___.

Pour modifier la détection du type mime sur un type __application__, il est possible d'ajouter une valeur à `File\Identify\FileIdentify::$typeInMime[] = 'mon-type-mime-custom'` si la seconde partie du mime __contient__ la valeur recherchée.
Note : attention à l'ordre dans cet _Array_, exemple : gzip est placé avant zip car sinon _zip_ aurait été détecté dans les deux cas.

Il est possible d'avoir des exceptions. Des cas où l'incohérence entre l'extension et le type mime est normale. Pour cela il faut ajouter la valeur comme ceci : `File\Identify\FileIdentify::$correctMismatch[] = ['ext' => 'mka', 'mime' => 'video/x-matroska', 'type' => 'audio']`
Dans cet exemple, l'extension mka qui est un fichier ___audio___ qui possède un type mime ___video___, avec cette valeur dans _$correctMismatch_, cette anomalie sera ignorée.

## Utilisation FileCorruptor

Créé pour fournir des données de test, vous pouvez néanmoins l'utiliser avec une des deux commandes si dessous :
```php
fwrite($File, File\Corruptor\FileCorruptor::lowLevelCorruption($filename));
fwrite($File2, File\Corruptor\FileCorruptor::highLevelCorruption($filename));
```

Comme leurs noms l'indiquent, ces deux fonctions offres des niveaux de corruption différents. Certains fichiers corrompus à faible niveau resteront lisibles ou réparables tandis que l'autre niveau de corruption modifie entièrement le fichier.